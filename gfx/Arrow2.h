/* C source file created by SevenuP v1.20                                */
/* SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain*/

/*
GRAPHIC DATA:
Pixel Size:      (  8,   8)
Char Size:       (  1,   1)
Frames:             4
Sort Priorities: Mask, Char line, Frame number
Data Outputted:  Gfx
Interleave:      Sprite
Mask:            Yes
*/

unsigned char Arrow2[64] = {
  0,  0,  2,  0,  6,  0, 14,  0,
  6,  0,  2,  0,  0,  0,  0,  0,
  0,  0,  4,  0, 12,  0, 28,  0,
 12,  0,  4,  0,  0,  0,  0,  0,
  0,  0,  8,  0, 24,  0, 56,  0,
 24,  0,  8,  0,  0,  0,  0,  0,
  0,  0, 16,  0, 48,  0,112,  0,
 48,  0, 16,  0,  0,  0,  0,  0};
