/*
    Cray-5 A Sinclair ZX Spectrum Game
    Copyleft (C) 2009 Luis I. Garcia Ventura

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#asm

; ATT_LOCATE: Devuelve en HL la dirección de atributos
; Entrada: BC Coordenadas: B = Línea C = Columna
; Salida: HL Dirección de atributos
; Se conservan DE y BC

ATT_LOCATE:
    ld a, b
    sra a
    sra a
    sra a
    add a, $58
    ld h, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld l, a
    ret


; DF_LOCATE: Devuelve en HL la dirección del archivo de pantalla
; Entrada: BC Coordenadas: B = Línea C = Columna
; Salida: HL Dirección del archivo de pantalla
; Se conservan DE y BC

DF_LOCATE:
    ld a, b
    and $f8
    add a, $40
    ld h, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld l, a
    ret

; DRAWGFX: Dibuja un gráfico en pantalla (Tiene que estar por líneas y en zigzag)
; ENTRADA: HL puntero al gráfico, BC Coordenadas en caracteres, DE: D = Caract Ancho E = Caract. Alto

DRAWGFX:
     ld (DRGVAR1), bc
     ld (DRGVAR2), bc
     ld (DRGVAR3), de

     push de

     push hl

     DRAWGFX_B0:

     call DF_LOCATE
     ex de, hl
     pop hl                            ; Ahora tenemos en HL el origen y en DE el destino

     ld a, (DRGVAR3 + 1)
     ld b, a
     ld c, 4

     DRAWGFX_B1:

     ld a, (hl)
     ld (de), a
     inc hl
     inc de
     djnz DRAWGFX_B1

     ld a, (DRGVAR3 + 1)
     ld b, a
     dec de
     inc d

     DRAWGFX_B2:

     ld a, (hl)
     ld (de), a
     inc hl
     dec de
     djnz DRAWGFX_B2

     ld a, (DRGVAR3 + 1)
     ld b, a
     inc de
     inc d

     dec c
     jp nz, DRAWGFX_B1


     ld bc, (DRGVAR1)
     inc b
     ld (DRGVAR1), bc


     pop bc               ; Y en BC las dimensiones
     dec c
     push bc
     push hl
     ld bc, (DRGVAR1)
     jp nz, DRAWGFX_B0

     ld bc, (DRGVAR2)


     pop hl

     pop af                 ; Le debemos una a la pila :P


     DRAWGFX_B3:

     push bc

     push hl
     call ATT_LOCATE
     ex de, hl
     pop hl                 ; Igual que antes: HL = Origen, DE = Destino


     ld bc, (DRGVAR3)          ; Ahora tenemos en BC las dimensiones


     DRAWGFX_B4:

     ld a, (hl)
     ld (de), a
     inc hl
     inc de

     djnz DRAWGFX_B4

     dec c
     jp z, DRAWGFX_Exit

     ld a, c
     ld (DRGVAR3), a

     pop bc
     inc b
     ld a, (DRGVAR2)
     ld c, a                            v
     jp DRAWGFX_B3

DRAWGFX_Exit:
     pop af
     ret

; DFL_CALLER: Devuelve en HL la direcci�n del scan solicitado
; Entrada:
;           B = Scan
;           C = Columna

DFL_CALLER:

     ld a, b
     and $c0
     rra
     scf
     rra
     rrca
     xor b
     and $f8
     xor b
     ld h, a

     ld a,b
     and $38
     rlca
     rlca
     add c
     ld l,a

     ret


; CSCROLL_UP: Rota una columna HACIA ARRIBA un p�xel
; ENTRADA:
;          C: Columna

CSCROLL_UP:

     ld h, $41
     ld l, c          ; A�ADIR COLUMNA
     ld a, (hl)
     ld (CSCROLL_Stack), a
     inc l
     inc l
     ld a, (hl)
     ld (CSCROLL_Stack + 1), a

     ld b, 1

     CSCU_B:

     ld a, b
     cp 191
     jr nc, CSCU_EXIT


     call DFL_CALLER
     ex de, hl
     inc b
     inc b
     call DFL_CALLER           ; ahora tenemos en HL la linea "origen" y en DE la linea "destino"


     ld a, (hl)
     ld (de), a

     inc l
     inc l
     inc e
     inc e
     ld a, (hl)
     ld (de), a

     jr CSCU_B

     CSCU_EXIT:

     ld h, $57
     ld a, $E0
     add a, c
     ld l, a      ; A�ADIR COLUMNA
     ld a, (CSCROLL_Stack)
     ld (hl), a
     inc l
     inc l
     ld a, (CSCROLL_Stack + 1)
     ld (hl), a

     ret

; CSCROLL_DOWN: Rota una columna HACIA ABAJO un p�xel
; ENTRADA:
;          C: Columna

CSCROLL_DOWN:


     ld h, $57
     ld a, $E0
     add a, c
     ld l, a      ; A�ADIR COLUMNA
     ld a, (hl)
     ld (CSCROLL_Stack), a
     inc l
     inc l
     ld a, (hl)
     ld (CSCROLL_Stack + 1), a

     ld b, 191

     CSCD_B:

     ld a, b
     cp 1
     jr z, CSCD_EXIT


     call DFL_CALLER
     ex de, hl
     dec b
     dec b
     call DFL_CALLER           ; ahora tenemos en HL la linea "origen" y en DE la linea "destino"

     ld a, (hl)
     ld (de), a

     inc l
     inc l
     inc e
     inc e
     ld a, (hl)
     ld (de), a

     jr CSCD_B



     CSCD_EXIT:

     ld h, $41
     ld l, c          ; A�ADIR COLUMNA
     ld a, (CSCROLL_Stack)
     ld (hl), a
     inc l
     inc l
     ld a, (CSCROLL_Stack + 1)
     ld (hl), a

     ret

FADE_OUT:
;// El segundo programa de m�s de 5 lineas en Z80 ASM de na_th_an
;/* Optimizado por Metalbrain */
  ld b, 7

  FADE_BUCLE0:

;/* Esto "oscurece" un paso la pantalla. Llamado 8 veces la dejar� en negro */

  push bc

  ld  e, 3            ; 3 tercios
  ld  hl, 22528       ; aqu� empiezan los atributos
  halt                ; esperamos retrazo.
fade_out_bucle:
  ld  a, (hl)         ; nos traemos el atributo actual

  ld  d, a            ; tomar atributo
  and  7              ; aislar la tinta
  jr  z, ink_done     ; si vale 0, no se decrementa
  dec  a              ; decrementamos tinta
ink_done:
  ld  b, a            ; en b tenemos ahora la tinta ya procesada.

  ld  a, d            ; tomar atributo
  and  56             ; aislar el papel, sin modificar su posici�n en el byte
  jr  z, paper_done   ; si vale 0, no se decrementa
  sub 8               ; decrementamos papel restando 8
paper_done:
  ld  c, a            ; en c tenemos ahora el papel ya procesado.
  ld  a, d
  and  192            ; nos quedamos con bits 6 y 7 (BRIGHT y FLASH)
  or  c               ; a�adimos paper
  or  b               ; e ink, con lo que recompuesto el atributo
  ld  (hl),a          ; lo escribimos,
  inc  l              ; e incrementamos el puntero.
  jr  nz, fade_out_bucle ; continuamos hasta acabar el tercio (cuando L valga 0)
  inc  h              ; siguiente tercio
  dec  e
  jr  nz, fade_out_bucle ; repetir las 3 veces

  pop bc

  djnz FADE_BUCLE0

  ret

; CHARBYCHAR: Modifica los atributos para mostrar un texto oculto, letra a letra + SFX
; ENTRADA:
;           BC Coordenadas: B = Línea C = Columna
;           A: N� Caracteres hacia la derecha

CHARBYCHAR:

    push af

    call ATT_LOCATE

    pop bc


    TEXT_LINE:

    ld a, (hl)
    and 64
    add a, 5
    ld (hl), a
    inc hl

;    di
    push bc
    push hl
    ld b, 1
    call SetRAMBank
    ld bc, 0
    call INICIAEFECTO
;    ld b, 0
    call SetRAMBack
    pop hl
    pop bc
;    ei

    halt
    halt
    halt
    halt

    djnz TEXT_LINE

    ret

CHARBYCHAR_TE:

   ld hl, 22528 + 7 * 32 + 6
   ld b, 8

   CBC_TE_C:

   ld de, 31

   ld (hl), 69
   inc hl
   ld (hl), 69
   add hl, de
   ld (hl), 69
   inc hl
   ld (hl), 69

   sbc hl, de

   halt
   halt
   halt
   halt

   djnz CBC_TE_C
   
   ret

FOTE_CHANGER:
    ld a, (hl)
    and 7
    cp 2

    ret c

    dec (hl)

    ret

FADE_OUT_THEEND:

    ld b, 10
    
    FOTE_HaltCycle:
    
    halt
    djnz FOTE_HaltCycle

    ld hl, 22528 + 32 + 7

    ld c, 15

    FOTE_B1:

    ld b, 14

    FOTE_B2:

    call FOTE_CHANGER

    inc hl
    djnz FOTE_B2

    ld de, 18
    add hl, de

    dec c
    jr nz, FOTE_B1
    
    ld hl, 22528 + 32 * 15 + 6
    call FOTE_CHANGER

    ld hl, 22528 + 32 * 16 + 11
    call FOTE_CHANGER
    ld hl, 22528 + 32 * 16 + 12
    call FOTE_CHANGER
    ld hl, 22528 + 32 * 16 + 13
    call FOTE_CHANGER
    ld hl, 22528 + 32 * 16 + 14
    call FOTE_CHANGER
    ld hl, 22528 + 32 * 16 + 15
    call FOTE_CHANGER

    ld hl, 22528 + 32 * 17 + 13
    call FOTE_CHANGER

    ret

SCR_THEEND:

    ld bc, 0

    ld hl, $4000

    SCR_TE_CYCLE:

    ex de, hl

    call DFL_CALLER

    push bc

    ld bc, $0015 ; 21 en decimal
    ldir

    pop bc

    call DFL_CALLER

    inc b

    ld a, b
    cp 192
    jp nz, SCR_TE_CYCLE

    ret

;//  -------------------------------------------

MENU_ATTRCYCLE:

    ld a, (22756 + 8)

    ld hl, 22756 + 7; 16384 + 1024 * 6 + 7 * 32 + 4
    ld de, 22756 + 8
    ld bc, 8
    lddr

    ld (22756), a

    ld a, (22788 + 8)

    ld hl, 22788 + 7; 16384 + 1024 * 6 + 8 * 32 + 4
    ld de, 22788 + 8
    ld bc, 8
    lddr

    ld (22788), a

    ret

;//  -------------------------------------------

FRAME_FX:

    ld bc, 0
    call CSCROLL_UP
    ld bc, 1
    call CSCROLL_DOWN
    ld bc, 28
    call CSCROLL_UP
    ld bc, 29
    call CSCROLL_DOWN



    ld a, (CURSOR_PAUSE)
    add a, 8
    ld (CURSOR_PAUSE), a
    cp 127

    ld a, 0
    jp c, CURSOR_OFF

    ld a, 4

    CURSOR_OFF:

    ld (16384 + 1024 * 6 + 3 * 32 + 18), a
    ld (16384 + 1024 * 6 + 3 * 32 + 19), a

    ret

#endasm

