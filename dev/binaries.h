/*
    Cray-5 A Sinclair ZX Spectrum Game
    Copyleft (C) 2009 Luis I. Garcia Ventura

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#asm
._Tile0
binary "../gfx/cray5-st_tileset.bin"
._SuperTile_0
binary "../gfx/cray5-st_01.bin"
._SuperTile_71
binary "../gfx/cray5-st_02.bin"
._SuperTile_80
binary "../gfx/cray5-st_03.bin"
._SuperTile_105
binary "../gfx/cray5-st_04.bin"
#endasm
