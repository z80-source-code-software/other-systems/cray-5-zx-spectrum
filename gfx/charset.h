/* C source file created by SevenuP v1.20                                */
/* SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain*/

/*
GRAPHIC DATA:
Pixel Size:      (256, 192)
Char Size:       ( 32,  24)
Sort Priorities: Char line, X char, Y char
Data Outputted:  Gfx
Interleave:      Sprite
Mask:            No
*/

unsigned char Charset[] = {
  0,  0,126, 66, 70, 70,126,  0,
  0,  0, 24,  8,  8, 28, 28,  0,
  0,  0,126,  6,126, 64,126,  0,
  0,  0,124,  4,126,  6,126,  0,
  0,  0, 96,102,126,  6,  6,  0,
  0,  0,126, 64,126,  6,126,  0,
  0,  0,124, 64,126, 70,126,  0,
  0,  0,126,  6, 12, 24, 24,  0,
  0,  0, 60, 36,126,102,126,  0,
  0,  0,126, 66,126,  6,  6,  0,
};
