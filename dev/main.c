/*
    Cray-5 A Sinclair ZX Spectrum Game
    Copyleft (C) 2009 Luis I. Garcia Ventura

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "main.h"
#include "asm.h"

/* MusicISR */
#asm

defw 0                        // 2 free bytes

#endasm

void ISR(void)
{
    #asm
;    di
    ld b,1
    call SetRAMBank
    call WYZPLAYERISR
;    ld b, 0
    call SetRAMBack
;    ei
    #endasm

    if (LetTrans)
       if (++Ticks == 20) {
           Ticks = 0;
           sp_PrintAtInv (1, 21, ColorTime, 246);
           sp_PrintAtInv (1, 22, ColorTime, 246);
           sp_PrintAtInv (1, 23, ColorTime, 246);
           sp_PrintAtInv (1, 24, ColorTime, 246);
           sp_PrintAtInv (1, 25, ColorTime, 246);
           PrintAtNum (1, 25, ColorTime, --Time);
           }
    return_nc(0);
}

void SetRAMBank()
{
#asm
    .SetRAMBank
    di
    xor a
    ld i,a
    ld  A, ($5B5C)
    and F8h
    or  B
    ld  BC, $7FFD
    ld  ($5B5C), A
    out (C), A
#endasm
}

void SetRAMBack()
{
    #asm
    .SetRAMBack
    ld  A, ($5B5C)
    and F8h
    ld  BC, $7FFD
    ld  ($5B5C), A
    out (C), A
    ld a,0F0h
    ld i,a
    ei
    #endasm
}

void wyz_play_sound (uchar fx_number)
{
    asm_int = 2 + (fx_number /*<< 8*/ * 256);
	#asm
;        di
        ld b,1
        call SetRAMBank

        ld bc, (_asm_int)
        ld a, (_asm_int)
        call INICIAEFECTO

;        ld b,0
        call SetRAMBack
;        ei
	#endasm
}

void wyz_play_music (uchar song_number)
{
     asm_int = song_number;

     #asm
;         di
         ld b, 1
         call SetRAMBank
         ld a, (_asm_int)
         call CARGA_CANCION
;         ld b, 0
         call SetRAMBack
;         ei
     #endasm
}

void wyz_stop_sound ()
{
    #asm
;    di
    ld b,1
    call SetRAMBank
    call SILENCIA_PLAYER
;    ld b,0
    call SetRAMBack
;    ei
    #endasm
}

void AddColour(struct sp_CS *cs)
{
   if (pi)
       ka = pi;
   else
       ka = (3 + (Enemy[n].Sort & 3));
   cs->colour = BRIGHT | ka;
}

void AddEnemy (unsigned char x, unsigned char y, unsigned char Kind)
{
  n = 0;
  while (Enemy[n].Sort != 255)
        n++;

  Enemy[n].Sort = Kind;
  pi = 0;
  sp_IterateSprChar(Enemy[n].Sprite, AddColour);
  Enemy[n].Frame = 0;
  Enemy[n].CurFrame = Enemies + 288 * Enemy[n].Sort;
  Enemy[n].x = (x << 3) + 4;
  Enemy[n].y = (y << 3) + 4;
  Enemy[n].KF = n + 1;
  Enemy[n].VelY = 0;
  if (Enemy[n].Sort > 4)
    Enemy[n].VelX = 0;
   else
    Enemy[n].VelX = VELENEMY / 2;
  sp_MoveSprAbs (Enemy[n].Sprite, GameField, Enemy[n].CurFrame - Enemy[n].PrevFrame,
                 y, x, 4, 4);
  Enemy[n].PrevFrame = Enemy[n].CurFrame;
}

void PrintSuperTile (unsigned char y, unsigned char x, unsigned char STile)
{
     if (STile > 245) {
        AddEnemy (x, y, STile - 246);
        return;
    }

     if (STile < 144)
         pp = SuperTile_0;
       else
         if (STile < 160) {
             pp = SuperTile_71;
             STile -= 144;
         }
           else
             if (STile < 242) {
                 pp = SuperTile_80;
                 STile -= 160;
             }
               else {
                 pp = SuperTile_105;
                 STile -= 242;
               }

     p = (BRIGHT | PAPER_BLUE) * (!y);

     pp += STile * 18;

     sp_PrintAtInv (y, x, *(pp+1) | p, *pp);
     sp_PrintAtInv (y, x + 1, *(pp+3) | p, *(pp+2));
     sp_PrintAtInv (y, x + 2, *(pp+5) | p, *(pp+4));
     sp_PrintAtInv (y + 1, x, *(pp+7) | p, *(pp+6));
     sp_PrintAtInv (y + 1, x + 1, *(pp+9) | p, *(pp+8));
     sp_PrintAtInv (y + 1, x + 2, *(pp+11) | p, *(pp+10));
     sp_PrintAtInv (y + 2, x, *(pp+13) | p, *(pp+12));
     sp_PrintAtInv (y + 2, x + 1, *(pp+15) | p, *(pp+14));
     sp_PrintAtInv (y + 2, x + 2, *(pp+17) | p, *(pp+16));
}

void Act_Map ()
{
    pp = MAP + 70 * ActRoom;

  #asm

;    di
    ld b, 3
    call SetRAMBank

    ld hl, _RoomBuffer
    ld de, (_pp)
    ld bc, 70
    ldir

;    ld b, 0
    call SetRAMBack
;    ei
  #endasm
}


unsigned char GetSTile (unsigned char y, unsigned char x)
{
    return RoomBuffer [(x / 24) * 7 + (y / 24)];
}

void PutSTile (unsigned char y, unsigned char x, unsigned char St)
{
    RoomBuffer [(x / 24) * 7 + (y / 24)] = St;
    PrintSuperTile (3 + (y / 24) * 3, 1 + (x / 24) * 3, St);
}

void DelParticle (unsigned char NumbP)
{
  sp_MoveSprAbs (Particle[NumbP].Sprite, GameField, 0, 0, 0, 0, 0);
  Particle[NumbP].Sort = 255;
}

void ClearSprites()
{
     for (m = 0; m < MAXPARTICLE; m++)
        DelParticle (m);

    for (m = 0; m < MAXFIRE; m++)
        if (*(Fire[m].Sprite))
            sp_MoveSprAbs (Fire[m].Sprite, GameField, 0, 0, 0, 0, 0);

     for (m = 0; m < MAXENEMY; m++)
         if (Enemy[m].Sort != 255) {
            Enemy[m].Sort = 255;
            sp_MoveSprAbs (Enemy[m].Sprite, GameField, 0, 0, 0, 0, 0);
        }
}

void ShowRoom ()
{
     Astronaut.LastX = Astronaut.x;
     Astronaut.LastY = Astronaut.y;

     ClearSprites();

     sp_ClearRect(GameField, BRIGHT | INK_WHITE, 0, sp_CR_TILES);
     sp_Invalidate(GameField, GameField);

     Astronaut.x = Astronaut.LastX;
     Astronaut.y = Astronaut.LastY;

     pp = MAP + 70 * ActRoom;

         #asm
;         di
         ld b, 3
         call SetRAMBank

         ld hl, (_pp)
         ld de, _RoomBuffer
         ld bc, 70
         ldir

;         ld b, 0
         call SetRAMBack
;         ei
         #endasm

     for (m = 0; m < 70; m++)
         if (RoomBuffer[m])
            PrintSuperTile (3 + (m % 7) * 3, 1 + (m / 7) * 3, RoomBuffer[m]);
}

void InitMap ()
{
    #asm
;        di
        ld b, 3
        call SetRAMBank

        ld de, MAP
        ld hl, 54052
        ld bc, 4900
        ldir

;        ld b, 0
        call SetRAMBack
;        ei
    #endasm
}

void InitTileSet ()
{
     for (n = 0; n < 244; n++)
        sp_TileArray (n, Tile0 + (n*8));

     for (n = 0; n < 10; n++)
        sp_TileArray (n + 246, Charset + (n*8));
}

void Init ()
{
    #asm
        ld b, 3
        call SetRAMBank

        ld hl, MAP
        ld de, 54052
        ld bc, 4900
        ldir

        ld b, 1
        call SetRAMBank
        ld a, (_asm_int)
        call $c000            ; Init WyzPlayer


;        ld b, 0
        call SetRAMBack
;        ei
    #endasm

    sp_Border (BLACK);
    sp_Initialize (INK_BLACK | PAPER_BLACK, ' ');
    sp_UpdateNow();

    #asm
        di
    #endasm

    sp_InitIM2 (0xf2f2);  // f1f1
    sp_CreateGenericISR (0xf2f2);  // f1f1
    sp_RegisterHookLast (255, ISR);

    InitTileSet();

    sp_AddMemory(0, 76, 14, AD_FREE);

    for (n = 0; n < MAXPARTICLE; n ++) {
//        Particle[n].CurFrame = Particle3;
        Particle[n].PrevFrame = Particle3;
        Particle[n].Sort = 255;
        Particle[n].Sprite = sp_CreateSpr (sp_XOR_SPRITE, 1, Particle3, 3, TRANSPARENT);
//        sp_MoveSprAbs (Particle[n].Sprite, GameField, 0, 0, 0, 0, 0);
        }

    for (n = 0; n < MAXFIRE; n ++) {
        Fire[n].Sprite = sp_CreateSpr (sp_XOR_SPRITE, 2, FireSpr, 3, TRANSPARENT);
//        sp_MoveSprAbs (Fire[n].Sprite, GameField, 0, 0, 0, 0, 0);
        }

    for (n = 0; n < MAXENEMY; n ++) {
//        Enemy[n].CurFrame = Enemies;
        Enemy[n].PrevFrame = Enemies;
        Enemy[n].Sort = 255;
        Enemy[n].Sprite = sp_CreateSpr (sp_OR_SPRITE, 3, Enemies, 2, TRANSPARENT);
        sp_AddColSpr (Enemy[n].Sprite, Enemies + 3 * 16, 70);
        sp_AddColSpr (Enemy[n].Sprite, Enemies + 6 * 16, 70);
//        sp_MoveSprAbs (Enemy[n].Sprite, GameField, 0, 0, 0, 0, 0);
        }

//    DeadAstronaut.CurFrame = Explosion;
    DeadAstronaut.PrevFrame = Explosion;
    DeadAstronaut.Sprite = sp_CreateSpr (sp_OR_SPRITE, 3, Explosion, 1, INK_RED | BRIGHT);
    sp_AddColSpr (DeadAstronaut.Sprite, Explosion + 3 * 16, INK_RED | BRIGHT);
    sp_AddColSpr (DeadAstronaut.Sprite, Explosion + 6 * 16, INK_RED | BRIGHT);
//    sp_MoveSprAbs (DeadAstronaut.Sprite, GameField, 0, 0, 0, 0, 0);

    for (n = 0; n < 2; n++) {
        SelectData.CurFrame = Arrows;
        SelectData.PrevFrame = Arrows;
        Selector[n].Sprite = sp_CreateSpr (sp_LOAD_SPRITE, 2, Arrows + n, 1, INK_MAGENTA | BRIGHT);
//        sp_MoveSprAbs (Selector[n].Sprite, TransFrame, 0, -1, -1, 0, 0);
    }

   Astronaut.Sprite = sp_CreateSpr (sp_OR_SPRITE, 4, Astronaut1, 1, TRANSPARENT);
   sp_AddColSpr (Astronaut.Sprite, Astronaut1 +  4 * 16, TRANSPARENT);
   sp_AddColSpr (Astronaut.Sprite, Astronaut1 +  8 * 16, TRANSPARENT);
   Astronaut.PrevFrame = Astronaut1;

/*    PrintAtNum (1, 25, 71, sp_LookupKey('p'));
    sp_UpdateNow();

    #asm

    di
    halt

    #endasm
    */

   Keys.up    = 507; // sp_LookupKey('q');
   Keys.down  = 509; // sp_LookupKey('a');
   Keys.left  = 735; // sp_LookupKey('o');
   Keys.right = 479; // sp_LookupKey('p');
   Keys.fire  = 383; //sp_LookupKey(' ');

   PreMenu();

   wyz_play_music (0);

/*
    wyz_play_music (0);

    #asm

    ; Show Gfx
;    di
    ld b, 4
    call SETRAMBANK
    ld hl, GFXMENU
    ld de, 16384
    call DEEXO

;    ld b, 0
    call SetRAMBack
;    ei

    #endasm
*/}

void PrintAtNum (uchar y, uchar x, uchar PColor, unsigned int Num)
{
    if (!Num)
       sp_PrintAtInv (y, x, PColor, 246);

    while (Num) {
          sp_PrintAtInv (y, x--, PColor, 246 + (Num % 10));
          Num /= 10;
          }
}


void NewLife ()
{
/*
    LetChange = 1;
    LetTrans = 1;
    Energy = 255;
    GameIsOver = 0;

    Astronaut.x = Astronaut.LastX;
    Astronaut.y = Astronaut.LastY;
    Astronaut.CurFrame = Astronaut1;
    Astronaut.VelX = 0;
    Astronaut.VelY = 0;
    Astronaut.Facing = 0;
    Astronaut.Frame = 0;
*/


     #asm
        ld de, (_Astronaut+4)
        ld (_Astronaut), de
        ld de, (_Astronaut+6)
        ld (_Astronaut+2), de
        ld de, _Astronaut1
        ld (_Astronaut+14), de

        xor a
        ld (_GameIsOver), a
        ld (_Astronaut+8), a
        ld (_Astronaut+9), a
        ld (_Astronaut+10), a
        ld (_Astronaut+11), a
        ld (_Astronaut+17), a
        ld (_Astronaut+18), a
;       inc a
        ld (_LetChange), a
        ld (_LetTrans), a
;        ld a, $ff
        dec a
        ld (_Energy), a

        ld hl, $4307    ; Restore energy bar
        ld de, $4308
        ld bc, 17
        ld (hl), $ff
        ldir
        ld hl, $4307
        ld de, $4407
        ld bc, 18
        ldir
    #endasm

    PrintAtNum (0, 29, INK_MAGENTA | BRIGHT, Lives);
    ShowRoom ();
}

void InitGame ()
{
    #asm
    ; Show game frame
;    di
    ld b, 4
    call SETRAMBANK
    ld hl, GFXFRAME
    ld de, 16384
    call DEEXO
;    ld b, 0
    call SetRAMBack
;    ei
    #endasm

    Astronaut.LastX = 32 << SENS;
    Astronaut.LastY = 48 << SENS;
    Time = 10000;
    ColorTime = INK_YELLOW | BRIGHT;

    Lives = 6;

    #asm
        xor a
        ld (_ActRoom), a
        ld (_Switchers), a
        ld (_Score), a
        ld (_Score+1), a
        ld (_CurrentObj), a
        inc a
        ld (_Level), a
    #endasm

    Astronaut.CurFrame = Astronaut1;
    InitMap();
    NewLife();
    PrintAtNum (1, 25, INK_YELLOW | BRIGHT, Time);

    sp_UpdateNow();

}

void GetScore (unsigned char Sc)
{
    Score += Scores [Sc];
    PrintAtNum (1, 12, INK_YELLOW | BRIGHT, Score);
}

void GameOver ()
{
    #asm
    call FADE_OUT

    ; Show Gfx
;    di
    ld b, 4
    call SETRAMBANK
    ld hl, GFXGAMEOVER
    ld de, 16384
    call DEEXO

;    ld b, 0
    call SetRAMBack
;    ei

    ld bc, $150b
    ld a, $0a
    call CHARBYCHAR

;    di
    ld b, 1
    call SetRAMBank
    ld a, 1
    call CARGA_CANCION

;    ld b, 0
    call SetRAMBack
;    ei


    xor a
    ld (_pi), a

    GO_CYCLE:

    ld hl, CURSOR_PAUSE
    inc (hl)
    ld a, (hl)
    cp 127
    jp c, GO_CHECKKEY

    halt
    halt
    halt
    halt

    ld hl, 22528 + 32 * 21 + 20
    ld a, (hl)
    xor 5
    ld (hl), a

    ld hl, _pi
    inc (hl)
    ld a, (hl)
    and a
    jp z, FADE_OUT

    GO_CHECKKEY: //    ; if there's no key pressed, repeat cycle again

    xor a
    in a, ($fe)
    cpl
    and $1f
    jp z, GO_CYCLE

    call FADE_OUT
    #endasm

    wyz_play_music (0);
}

void PlayerDamage (char Damage)
{
    #asm
        ld hl, _Sent
        ld (hl), -1
    #endasm

//    Sent = -1

    if (Damage < 0) {
       #asm
          ld a, 1
          ld (_Sent), a
       #endasm
       Damage = -Damage;
    }
     else
       if (MagnetSound == 0)
          wyz_play_sound (FXDAMAGE);

    while (Damage--) {
    pi = 143 - ((143 * Energy) / 255);
    ka = pi >> 3;
    pi = pi & 7;

    #asm
        ld hl, _pi
        ld b, (hl)
        inc b
        xor a

        .pd
        scf
        rla
        djnz pd

        cpl

        ld hl, _ka
        ld b,0
        ld c, (hl)
        ld hl, $4318
        and a
        sbc hl, bc

        ld b, 2
        .pri
        ld (hl), a
        inc h
        djnz pri

        ld a, 7
        cp l
        jp z, exit


        dec h
        dec l
        ld b, 2
        .sec
        ld (hl), $ff
        dec h
        djnz sec

        .exit

    #endasm
    }
     #asm
/*    if (Energy == 255 && Sent > 0)
       Sent = 0;

    Energy += Sent;

*/
    ld hl, _Sent
    ld a, (_Energy)
    cp $ff
    jp nz, DM_AfterIf
    ld b, (hl)
    bit 7, b
    jp nz, DM_AfterIf
    ld (hl), 0

    DM_AfterIf:
    add a, (hl)
    ld (_Energy), a
    #endasm
}

void CheckIfDead ()
{

    if (Energy == 0) {
        DeadAstronaut.KF = 20;
        DeadAstronaut.Frame = 0;
        DeadAstronaut.CurFrame = Explosion;
        wyz_play_sound (FXEXPLOSION);

        sp_MoveSprAbs (DeadAstronaut.Sprite, GameField, DeadAstronaut.CurFrame - DeadAstronaut.PrevFrame,
                       Astronaut.y >> (SENS + 3), Astronaut.x >> (SENS + 3), (Astronaut.x >> SENS) & 7, ((Astronaut.y >> SENS) & 7 + 4));
        sp_MoveSprAbs (Astronaut.Sprite, GameField, 0, 0, 0, 0, 0);
        DeadAstronaut.PrevFrame = Explosion;

        Astronaut.x = 0;
        Astronaut.y = 0;

        while (DeadAstronaut.Frame < 7)
           if (--DeadAstronaut.KF == 0) {
             DeadAstronaut.KF = 20;
              if (++DeadAstronaut.Frame < 7) {
                 DeadAstronaut.CurFrame = Explosion + EnemiesDiff[DeadAstronaut.Frame];
                 sp_MoveSprRel (DeadAstronaut.Sprite, GameField, DeadAstronaut.CurFrame - DeadAstronaut.PrevFrame, 0, 0, 0, 0);
                 DeadAstronaut.PrevFrame = DeadAstronaut.CurFrame;
                 ActEnemies ();
                 ActParticles();
//                 ActFire();
                 sp_UpdateNow();
                 }
           }

        sp_MoveSprAbs (DeadAstronaut.Sprite, GameField, 0, 0, 0, 0, 0);
        ClearSprites();
        Act_Map();
        if (--Lives != 255)
            NewLife();
         else
            GameIsOver = 1;
        }
}

void StopPlayer() {
    Astronaut.VelX = 0;
    Astronaut.VelY = 0;
    if (Astronaut.Facing < 2)
        Astronaut.CurFrame = Astronaut1 + 193 * Astronaut.Facing;
}

void teleport()
{
    #asm
        xor a
        ld (_LetTrans), a
        ld (_ka), a
    #endasm

    ClearSprites();
    sp_MoveSprAbs (Astronaut.Sprite, GameField, 0, -6, -6, 0, 0);
    sp_ClearRect(TransFrame, BRIGHT | INK_WHITE, 0, sp_CR_TILES);
    sp_Invalidate(TransFrame, TransFrame);
    sp_UpdateNow();

    for (n = 1; n < 9; n++)
        if (Switchers >= SwitchsNeeded[n])
           ka = n;

    #asm

    ld hl, 22528 + 3 * 32
    ld de, 22529 + 3 * 32
    ld bc, 767 - 3 * 32
    ld (hl), 1
    ldir

;    di
    ld b, 4
    call SETRAMBANK

    ld bc, 2057  //;$0809
    ld hl, GFXTRANSP
    ld de, $0f0b
    call DRAWGFX

;    ld b, 0
    call SetRAMBack
;    ei

    ld hl, _ka
    ld b, (hl)
    ld a, b
    and a
    jp z, exitgc
    ld de, 29
    ld hl, 22528 + 10 * 32 + 12

    .greencycle

    inc (hl)
    inc (hl)
    inc hl
    inc hl
    inc (hl)
    inc (hl)
    inc hl
    inc (hl)
    inc (hl)

    add hl, de
    djnz greencycle

    .exitgc

    ld bc, $0911
    ld a, 5
    call CHARBYCHAR

    LD a, 5
    LD bc, $0a11
    call CHARBYCHAR

    #endasm

    Astronaut.LastX = Astronaut.x;
    Astronaut.LastY = Astronaut.y;

    sp_MoveSprAbs (Selector[0].Sprite, TransFrame, 0, 8 + Level, 11, 0, 0);
    sp_MoveSprAbs (Selector[1].Sprite, TransFrame, 0, 8 + Level, 13, 0, 0);
    SelectData.Obj = 0;

    while (SelectData.Obj < 2) {
            k = JoyFunc (&Keys);

            if ((k & sp_UP) == 0)
                if (SelectData.Obj == 0 && (Selector[0].Sprite)->row > 9)
                   SelectData.Obj = -1;

            if ((k & sp_DOWN) == 0)
                if (SelectData.Obj == 0 && (Selector[0].Sprite)->row < 17 &&
                    Switchers >= SwitchsNeeded [(Selector[0].Sprite)->row - 8] )
                   SelectData.Obj = 1;

          SelectData.CurFrame += 32;
          if (SelectData.CurFrame >= Arrows + 128) {
             SelectData.CurFrame = Arrows;
             #asm
                 ld hl, 22528 + 32 * 10 + 21
                 ld a, (hl)
                 xor 5
                 ld (hl), a
             #endasm
          }

          sp_MoveSprRel (Selector[0].Sprite, TransFrame, SelectData.CurFrame - SelectData.PrevFrame, 0, 0, 0, SelectData.Obj << 1);
          sp_MoveSprRel (Selector[1].Sprite, TransFrame, SelectData.CurFrame - SelectData.PrevFrame, 0, 0, 0, SelectData.Obj << 1);

          if ((Selector[0].Sprite)->ver_rot == 0) {
             Level += SelectData.Obj;
             SelectData.Obj = 0;
            }

          sp_UpdateNow();

          sp_Wait (5);

          SelectData.PrevFrame = SelectData.CurFrame;

          if ((k & sp_FIRE) == 0)
            SelectData.Obj = 2;
    }
    sp_MoveSprAbs (Selector[0].Sprite, GameField, 0, -2, -2, 0, 0);
    sp_MoveSprAbs (Selector[1].Sprite, GameField, 0, -2, -2, 0, 0);

    Act_Map();
    ActRoom = SwitchLocation [Level - 1];

    ShowRoom ();
    PrintAtNum (1, 17, INK_YELLOW | BRIGHT, Level);
    wyz_play_sound (FXTELEPORT);
}

void GameFinished ()   // The End
{
  wyz_stop_sound ();
  LetTrans = 0;           // Don't actualize the timer count

  while (Energy) {
        PlayerDamage (2);
        Score += 10;
        PrintAtNum (1, 12, INK_YELLOW | BRIGHT, Score);
        sp_UpdateNow ();
        sp_Wait(1);
        }
  wyz_play_sound (1);

  sp_Wait (50);

  while (Lives) {
        Lives--;
        Score += 100;
        PrintAtNum (0, 29, INK_MAGENTA | BRIGHT, Lives);
        PrintAtNum (1, 12, INK_YELLOW | BRIGHT, Score);
        sp_UpdateNow ();
        wyz_play_sound (0);
        sp_Wait(50);
        }

  sp_Wait (50);

  Score += Time;
  PrintAtNum (1, 12, INK_YELLOW | BRIGHT, Score);
  wyz_play_sound (1);
  for (Time = 21; Time < 26; Time++)
      sp_PrintAtInv (1, Time, INK_YELLOW | BRIGHT, 246);
  sp_UpdateNow ();
  sp_Wait (100);



  #asm
    call FADE_OUT

    ; Show Gfx
    di
    ld b, 4
    call SETRAMBANK
    ld hl, GFXFINISHED
    ld de, 16384
    call DEEXO

    ld b, 1
    call SetRAMBank
    ld a, 1
    call CARGA_CANCION

;    ld b, 0
    call SetRAMBack
;    ei

    ld b, 25
    .wt
    halt
    djnz wt

    ld b, 7
    .FOTE
    push bc
    call FADE_OUT_THEEND
    pop bc
    djnz FOTE

    ; --------------------

    ld b, 50
    GFC_PAUSE:
    halt
    djnz GFC_PAUSE

;    di
    ld b, 4
    call SETRAMBANK

    ld hl, 22528 + 7 * 32 + 6
    ld de, 22528 + 7 * 32 + 7
    ld bc, $000f
    ld (hl), 0
    ldir

    ld hl, 22528 + 8 * 32 + 6
    ld de, 22528 + 8 * 32 + 7
    ld bc, $000f
    ld (hl), 0
    ldir

    ld bc, $0706  //;$0809
    ld hl, GFXBANNER
    ld de, $1002
    call DRAWGFX


;    ld b, 0
    call SetRAMBack
;    ei

    call CHARBYCHAR_TE

    ld c, 7

    GF_CURSOR:

    ld hl, 22528 + 8 * 32 + 20
    ld a, (hl)
    xor 69
    ld (hl), a
    inc hl
    ld (hl), a

    ld b, 20
    GF_CURSOR_W:
    halt
    djnz GF_CURSOR_W

    dec c
    jr nz, GF_CURSOR

    ; --------------------
    ; Clear the scroll area with the double courtain effect

    ld c, 0

    .b1
    ld e, $7f
    ld d, 4

    .b2
    xor a
    ld b, 192

    .b3
    push bc
    ld b, a
    call DFL_CALLER
    ld a, b
    pop bc
    inc a
    push af
    ld a, (hl)
    and e

    ld (hl), a
    pop af
    djnz b3

    scf
    rr e
    scf
    rr e
    dec d
    jr nz, b2

    inc c
    ld a, c
    cp 21
    jr nz, b1

    ld c, 20

    .bb1
    ld e, $fe
    ld d, 4

    .bb2
    xor a
    ld b, 192

    .bb3
    push bc
    ld b, a
    call DFL_CALLER
    ld a, b
    pop bc
    inc a
    push af
    ld a, (hl)
    and e

    ld (hl), a
    pop af
    djnz bb3

    scf
    rl e
    scf
    rl e
    dec d
    jr nz, bb2

    dec c
    jp p, bb1

    ; --------------------
    ; Set attributes to Scroll area

;    di
    ld b, 4
    call SETRAMBANK
    ld hl, ATTRSCR
    ld de, 22528
    ld bc, 768
    ldir

    ld b, 1
    call SetRAMBank
    ld a, 0
    call CARGA_CANCION

;    ld b, 0
    call SetRAMBack
;    ei

    ; --------------------
    ; Begin scroll

    ld hl, 0
    xor a     ; Frase 0, linea 0

    ld (DRGVAR1), hl
    ld (DRGVAR2), a

    GF_CYCLE:

    halt
;    di
    ld b, 3
    call SETRAMBANK

    ld hl, (DRGVAR1)
    ld a, (DRGVAR2)

    call PRINTSTR_LINE

    push af

;    ld b, 0
    call SetRAMBack
;    ei

    pop af
    jp c, THEEND_EXIT

    ld hl, DRGVAR2
    inc (hl)
    ld a, (DRGVAR2)

    cp 8
    call z, NEW_TEXT

    halt
    call SCR_THEEND

    GF_CHECKKEY: //    ; if there's no key pressed, repeat cycle again

    xor a
    in a, ($fe)
    cpl
    and $1f
    jp z, GF_CYCLE

    jp THEEND_EXIT

    NEW_TEXT:

    xor a
    ld (DRGVAR2), a
    ld hl, (DRGVAR1)
    inc hl
    ld (DRGVAR1), hl
    ret

    THEEND_EXIT:

    call FADE_OUT

    #endasm
}

void CheckSTCol (unsigned char x, unsigned char y)
{
     ST = GetSTile (y, x);

     if (ST == 0 || ST > 245)
        return;

     if (ST == 245) {
         PutSTile (y, x, 180);
         GetScore (2);
         wyz_play_sound (FXSWITCH);
         PrintAtNum (1, 30, INK_YELLOW | BRIGHT, ++Switchers);
         if (Switchers == 13) {
           #asm
;              di
              ld b, 3
              call SetRAMBank

              xor a
              ld (MAP + 70 * 69 + 9 * 7 + 3), a
              ld (MAP + 70 * 69 + 9 * 7 + 4), a
              ld (MAP + 70 * 69 + 9 * 7 + 5), a

;    ld b, 0
    call SetRAMBack
;    ei
           #endasm

            if (ActRoom == 69) {
              PutSTile (24 * 3, 24 * 9, 0);
              sp_UpdateNow();
              sp_Wait (10);
              PutSTile (24 * 4, 24 * 9, 0);
              sp_UpdateNow();
              sp_Wait (10);
              PutSTile (24 * 5, 24 * 9, 0);
              sp_UpdateNow();
            }

         }
         return;
         }

     if (ST > 241 && LetChange) {
        wyz_play_sound (FXKEY);

        PutSTile (y, x, CurrentObj);
        CurrentObj = ST;
        PrintSuperTile (0, 1, CurrentObj);
        LetChange = 0;
        }

     if (ST > 159)
        return;

     if (ST < 144)
         pp = SuperTile_0 + ST * 18;
       else
         if (ST < 160)
             pp = SuperTile_71 + (ST - 144) * 18;
           else
             pp = SuperTile_80 + (ST - 160) * 18;

     if (*(pp + ((((x % 24) >> 3) + 3 * ((y % 24) >> 3)) << 1)) == 0)
        return;

     if ((ST == 14 && CurrentObj == 244) || (ST == 13 && CurrentObj == 243) || (ST == 15 && CurrentObj == 242)) {
         wyz_play_sound (FXDOOR);
         CurrentObj = 0;
         PutSTile (y, x, 0);
         PrintSuperTile (0, 1, 0);
         GetScore (1);
         return;
     }

     if (ST > 143 && ST < 160) {
        PlayerDamage(2);
        CheckIfDead();
     }

     if (ST == 64)
       PlayerDamage (-2);

    if (ST == 23 && LetTrans)
        teleport();

    if (ST > 65 && ST < 71 && Switchers == 13)
       GameIsOver = 2;

    StopPlayer();
}

void CheckEnCol (unsigned char x, unsigned char y)
{
    x += 8;
    y += 24;
    if (Enemy[0].Sort < 11 &&
        x <= Enemy[0].x + 16 && x + 6 >= Enemy[0].x &&
        y <= Enemy[0].y + 16 && y + 6 >= Enemy[0].y) {
        PlayerDamage (4);
        CheckIfDead();
        StopPlayer();
        return;
    }
    if (Enemy[1].Sort < 11 &&
        x <= Enemy[1].x + 16 && x + 6 >= Enemy[1].x &&
        y <= Enemy[1].y + 16 && y + 6 >= Enemy[1].y) {
        PlayerDamage (4);
        CheckIfDead();
        StopPlayer();
        return;
    }
    if (Enemy[2].Sort < 11 &&
        x <= Enemy[2].x + 16 && x + 6 >= Enemy[2].x &&
        y <= Enemy[2].y + 16 && y + 6 >= Enemy[2].y) {
        PlayerDamage (4);
        CheckIfDead();
        StopPlayer();
    }
}

void ChangeRoom (signed char VarX, signed char VarY)
{
    Act_Map ();
    ActRoom += VarX + 5 * VarY;
}

void AddParticle (unsigned char x, unsigned char y)
{
  for (n = 0; n < MAXPARTICLE; n++)
    if (Particle[n].x == x && Particle[n].y == y)
        return;

  n = 0;
  while (n < MAXPARTICLE && Particle[n].Sort != 255)
        n++;

  if (n >= MAXPARTICLE)
    return;

  Particle[n].Sort = 2; //(Astronaut.x + Astronaut.y) % 2 + 1;
  Particle[n].Frame = 0;
/*  switch (Particle[n].Sort) {
         case 0:
              Particle[n].CurFrame = Particle1;
              break;
         case 1:
              Particle[n].CurFrame = Particle2;
              break;*/
//         case 2:
              Particle[n].CurFrame = Particle3;
//              break;
//              }
  Particle[n].x = x;
  Particle[n].y = y;
  Particle[n].KF = n + 1;
  sp_MoveSprAbs (Particle[n].Sprite, GameField, Particle[n].CurFrame - Particle[n].PrevFrame, Particle[n].y, Particle[n].x, 0, 0);
  Particle[n].PrevFrame = Particle[n].CurFrame;
}

void ActParticles ()
{
  for (m = 0; m < MAXPARTICLE; m++)
    if (Particle[m].Sort != 255)
      if (--Particle[m].KF == 0) {
         Particle[m].KF = 2;
         Particle[m].Frame += 1;
         if (Particle[m].Frame == 8) //PartLastFrame[Particle[m].Sort])
            DelParticle (m);
           else {
            Particle[m].CurFrame = Particle3/*PartInitFrame[Particle[m].Sort]*/ + 16 * (Particle[m].Frame >> 1) + Particle[m].Frame % 2;
            sp_MoveSprRel (Particle[m].Sprite, GameField, Particle[m].CurFrame - Particle[m].PrevFrame, 0, 0, 0, 0);
            Particle[m].PrevFrame = Particle[m].CurFrame;
            }
      }
}

void ActEnemies ()
{
  unsigned char x, y;

  for (o = 0; o < MAXENEMY; o++)
    if (Enemy[o].Sort != 255) {

      if (Enemy[o].Sort < 11) {

        if (Enemy[o].Sort > 4) {
           if (Enemy[o].y < Astronaut.y >> SENS)
               Enemy[o].VelY = VELENEMY;
           if (Enemy[o].y + 16 > (Astronaut.y >> SENS) + 24)
               Enemy[o].VelY = -VELENEMY;
        }

        x = Enemy[o].x + Enemy[o].VelX;
        y = Enemy[o].y + Enemy[o].VelY;

        if (x + 16 >= (Astronaut.x + Astronaut.VelX) >> SENS &&
            x <= ((Astronaut.x + Astronaut.VelX) >> SENS) + 16 &&
            y + 16 >= (Astronaut.y + Astronaut.VelY) >> SENS &&
            y <= ((Astronaut.y + Astronaut.VelY) >> SENS) + 24) { // There is a collision between player and enemy??
               Enemy[o].VelY = 0;
               Enemy[o].VelX *= -1;
               PlayerDamage(4);
               CheckIfDead();
               }

         if (Enemy[o].VelX > 0)
             x += 16;
         if (Enemy[o].VelY > 0)
             y += 16;

         x -= 8;
         y -= 24;

         ST = GetSTile (y, x);

         if ((ST > 0 && ST < 246) ||
             x >= 240 ||
             x < 8) {
             Enemy[o].VelY = 0;
             Enemy[o].VelX *= -1;
             }
    }

         if (--Enemy[o].KF == 0) {
             Enemy[o].KF = 2;
             Enemy[o].Frame += 1;
             if (Enemy[o].Sort != 11) {
                 if (Enemy[o].Frame == 4)
                     Enemy[o].Frame = 0;
                 Enemy[o].CurFrame = Enemies + 288 * Enemy[o].Sort;
                 }
              else {
             Enemy[o].VelX = 0;
                 Enemy[o].VelY = 0;
                 Enemy[o].CurFrame = Explosion;
                 if (Enemy[o].Frame == 7) {
                     Enemy[o].Sort = 255;
                     pi = 7;
                     sp_IterateSprChar(Enemy[o].Sprite, AddColour);
                     sp_MoveSprAbs (Enemy[o].Sprite, GameField, 0, 0, 0, 0, 0);
                     }
                 }
              if (Enemy[o].Frame < 7) {
                  Enemy[o].CurFrame +=  EnemiesDiff [Enemy[o].Frame]; // ((9 * (Enemy[o].Frame >> 1)) << 4) + (Enemy[o].Frame & 1);
              sp_MoveSprRel (Enemy[o].Sprite, GameField, Enemy[o].CurFrame - Enemy[o].PrevFrame,
                                 0,0,Enemy[o].VelX,Enemy[o].VelY);
                  Enemy[o].y += Enemy[o].VelY;
                  Enemy[o].x += Enemy[o].VelX;
                  Enemy[o].PrevFrame = Enemy[o].CurFrame;
              }
               }
          }
        else  {  // We need to waste a bit of time to keep the same game speed both with and without enemies.
          #asm
              ld b, 255
            .waiting
              nop
              nop
              nop
              nop
              nop
              nop
              nop
              nop
              nop
              nop
              nop
              nop
              djnz waiting
           #endasm
       }
}

void AddFire (unsigned char x, unsigned char y)
{
  n = 0;
  while (Fire[n].Sprite->row != 0 && n < MAXFIRE)
        n++;

  if (n >= MAXFIRE)
    return;

  wyz_play_sound (FXFIRE);

  Fire[n].VelX = -8 * (Astronaut.Facing * 2 - 1);
  sp_MoveSprAbs (Fire[n].Sprite, GameField, 0, y >> 3, x >> 3, Fire[n].VelX, y & 7);
}

void ActFire ()
{
  for (n = 0; n < MAXFIRE; n++)
    if (Fire[n].Sprite->row) {

        m = Fire[n].Sprite->col - 1 + (Fire[n].VelX >> 3);
        o = Fire[n].Sprite->row - 3;

        if (Fire[n].Sprite->ver_rot > 3)
           o++;

        if (m > 30)
           sp_MoveSprAbs (Fire[n].Sprite, GameField, 0, 0, 0, 0, 0);

         else {
            ST = GetSTile (o << 3, m << 3);
            if (ST > 0 && ST < 160) {

                   if (ST < 144)
                      pp = SuperTile_0 + ST * 18;
                     else
                      pp = SuperTile_71 + (ST - 144) * 18;


                pp += 6 * (o % 3) + 2 * (m % 3);

                if (*(pp))
                  sp_MoveSprAbs (Fire[n].Sprite, GameField, 0, 0, 0, 0, 0);
                  }

              else

                for (p = 0; p < MAXENEMY; p++)
                    if (Enemy[p].Sort < 11 &&

                        Enemy[p].x + 16 >= (Fire[n].Sprite->col) << 3 &&   // Faster than using sp_IntRect
                        Enemy[p].x <= (Fire[n].Sprite->col + 1) << 3 &&
                        Enemy[p].y + 16 >= (Fire[n].Sprite->row)<< 3 &&
                        Enemy[p].y <= (Fire[n].Sprite->row + 1) << 3
//                        sp_IntRect (Enemy[p].Sprite, Fire[n].Sprite, 0)
                         ) {
                            wyz_play_sound (FXEXPLOSION);
                            Enemy[p].Sort = 11;
                            Enemy[p].Frame = 0;
                            Enemy[p].KF = 1;
                            GetScore (0);
                            pi = 2;
                            sp_IterateSprChar(Enemy[p].Sprite, AddColour);
                            sp_MoveSprAbs (Fire[n].Sprite, GameField, 0, 0, 0, 0, 0);
                        }
            }

            sp_MoveSprRel (Fire[n].Sprite, GameField, 0, 0, 0, Fire[n].VelX, 0);
    }

}

void Gamecycle ()
{
    wyz_play_music (2);

    while (!GameIsOver && Time) {

      initcycle:

            k = (JoyFunc) (&Keys);

            if ((k & sp_RIGHT) == 0) {
                if (Astronaut.Facing == 1) {
                    Astronaut.Facing = 3;
                    Astronaut.Frame = 0;
                    Astronaut.KF = 3;
                    Astronaut.CurFrame = Astronaut2;
                }

                Astronaut.VelX += (Astronaut.VelX < (3 << SENS));
                AddParticle ((Astronaut.x + Astronaut.VelX >> (SENS + 3)) + (Astronaut.Facing != 0) + (Astronaut.VelX > 3), (Astronaut.y >> (SENS + 3)) + 3);
//                Astronaut.VelY -= (Astronaut.VelY > 0) - (Astronaut.VelY < 0);
                if (Astronaut.VelY > 0)
                    Astronaut.VelY--;
                   else
                    if (Astronaut.VelY < 0)
                        Astronaut.VelY++;
                }
              else
                if ((k & sp_LEFT) == 0) {
                   if (Astronaut.Facing == 0) {
                       Astronaut.Facing = 4;
                       Astronaut.Frame = 0;
                       Astronaut.KF = 3;
                    Astronaut.CurFrame = Astronaut2 + 193;
                   }
                   Astronaut.VelX -= (Astronaut.VelX > (-3 << SENS));
                   AddParticle ((Astronaut.x + Astronaut.VelX >> (SENS + 3)) + (Astronaut.Facing != 0) , (Astronaut.y >> (SENS + 3)) + 3);
                if (Astronaut.VelY > 0)
                    Astronaut.VelY--;
                   else
                    if (Astronaut.VelY < 0)
                        Astronaut.VelY++;
                    }
                  else {
                     Astronaut.VelY++;  // GRAVITY
                     if (Astronaut.VelX > 0)
                        Astronaut.VelX--;
                       else
                        if (Astronaut.VelX < 0)
                           Astronaut.VelX++;
                    }

            if ((k & sp_UP) == 0 && Astronaut.VelY > (-2 << SENS)) {
                Astronaut.VelY -= 2;
                if (Astronaut.VelY > 0)
                   Astronaut.VelY--;
                   AddParticle ((Astronaut.x >> (SENS + 3)) + (Astronaut.Facing != 0), (Astronaut.y >> (SENS + 3)) + 3);
                }

            if ((k & sp_FIRE) != 0)
                LetFire = 1;
              else
                if (LetFire && Astronaut.Facing < 2) {
                    AddFire(Astronaut.x >> SENS, (Astronaut.y >> SENS) + 8);
                    LetFire = 0;}

            if (Astronaut.Facing > 1)
               if (--(Astronaut.KF) == 0) {
                  Astronaut.KF = 2;
                  if (Astronaut.Facing == 4)
                      Astronaut.CurFrame = Astronaut2 + af[++(Astronaut.Frame) + 3];
                    else
                      Astronaut.CurFrame = Astronaut2 + af[++(Astronaut.Frame)];

                  if (Astronaut.Frame == 3)
                     Astronaut.Facing -= 3;
               }

            if (Astronaut.Facing < 2) {
              if (Astronaut.VelY < -1 || Astronaut.VelX >> SENS < -1 || Astronaut.VelX >> SENS > 1)
                  Astronaut.CurFrame = &Astronaut1[af [1 + 3 * Astronaut.Facing]];
                else
                  if (Astronaut.VelY >> (SENS > 1))
                      Astronaut.CurFrame = &Astronaut1[af [2 + 3 * Astronaut.Facing]];
                    else
                      Astronaut.CurFrame = &Astronaut1[af [3 * Astronaut.Facing]];
              }

           Ax = ((Astronaut.x + Astronaut.VelX) >> SENS) - 8;
           Ay = ((Astronaut.y + Astronaut.VelY) >> SENS) - 24;

          if (Ay > 250) {
              ChangeRoom (0, -1);
              Astronaut.y = 167 << SENS;
              ShowRoom ();
              goto initcycle;
              }
           else
              if (Ay > 167 - 24) {
                 ChangeRoom (0, 1);
                 Astronaut.y = 24 << SENS;
                 ShowRoom ();
                 goto initcycle;
                 }

          if (Ax > 250) {  // Exit by left side
              ChangeRoom (-1, 0);
              Astronaut.x = (255 - 16 - 8) << SENS;
              ShowRoom ();
              goto initcycle;
              }
           else
              if (Ax > 255 - 24 - 8)  // Exit by right side
                    if (ActRoom != 69) { // 69 is the last room. When the player already have turned off all the 13 switchers, the right wall is open, and player can try to go out trough that "hole"
                        ChangeRoom (1, 0);
                        Astronaut.x = 8 << SENS;
                        ShowRoom ();
                        goto initcycle;
                        }
                      else
                        Astronaut.VelX = 0; // If you are at room 69, then stop player!

           ActEnemies();

           if (Ay > 32) // If player is at the top of the screen, then you don't need to look for magnets above him
              if (GetSTile (Ay - 24 + 2, Ax + 2) == 148 || GetSTile (Ay - 24 - 24 + 2, Ax + 2) == 148)  { // Is there any magnet above player? (Using Ay - 16 instead of Ay - 24 the magnet will also stop player in horizontal movement)
                  Astronaut.VelY -= 2;
                  if (MagnetSound == 0)
                     wyz_play_sound (FXMAGNET);
                  if (++MagnetSound >= 30)
                        MagnetSound = 0;
                  }
                else
                  if (GetSTile (Ay - 24 + 2, Ax + 16 - 2) == 148 || GetSTile (Ay - 24 - 24 + 2, Ax + 16 - 2) == 148) { // Don't forget to check on player's column further to the right!
                      Astronaut.VelY -= 2;
                      if (MagnetSound == 0)
                         wyz_play_sound (FXMAGNET);
                      if (++MagnetSound >= 30)
                          MagnetSound = 0;
                      }
                    else
                      MagnetSound = 0;

           if (GetSTile (Ay, Ax + 2) == 148 || GetSTile (Ay, Ax + 16 - 2) == 148) { // Magnets are harmful
              PlayerDamage (2);
              CheckIfDead();
              Astronaut.VelY = 0;
              }

           if (Astronaut.VelX > 0) {
               CheckSTCol (Ax + 16 - 2, Ay + 2);
               CheckSTCol (Ax + 16 - 2, Ay + 8);
               CheckSTCol (Ax + 16 - 2, Ay + 16 - 2);
               CheckSTCol (Ax + 16 - 2, Ay + 24 - 2);

               CheckEnCol (Ax + 16 - 2, Ay + 8);
               CheckEnCol (Ax + 16 - 2, Ay + 16 - 2);
               }

           if (Astronaut.VelX < 0) {
               CheckSTCol (Ax + 2, Ay + 2);
               CheckSTCol (Ax + 2, Ay + 8);
               CheckSTCol (Ax + 2, Ay + 16 - 2);
               CheckSTCol (Ax + 2, Ay + 24 - 2);

               CheckEnCol (Ax + 2, Ay + 8);
               CheckEnCol (Ax + 2, Ay + 16 - 2);
               }

           if (Astronaut.VelY > 0) {
               CheckSTCol (Ax + 2, Ay + 24 - 2);
               CheckSTCol (Ax + 8, Ay + 24 - 2);
               CheckSTCol (Ax + 16 - 2, Ay + 24 - 2);

               CheckEnCol (Ax + 2, Ay + 24 - 2);
               CheckEnCol (Ax + 8, Ay + 24 - 2);
               CheckEnCol (Ax + 16 - 2, Ay + 24 - 2);
               }

          if (Astronaut.VelY < 0) {
               CheckSTCol (Ax + 2, Ay + 2);
               CheckSTCol (Ax + 8, Ay + 2);
               CheckSTCol (Ax + 16 - 2, Ay + 2);

               CheckEnCol (Ax + 2, Ay + 2);
               CheckEnCol (Ax + 8, Ay + 2);
               CheckEnCol (Ax + 16 - 2, Ay + 2);
               }

          Astronaut.x += Astronaut.VelX;
          Astronaut.y += Astronaut.VelY;

          if (!LetChange)    // If there's no any key around you, reset LetChange flag, so you could get the next key you'll find
              if (GetSTile (Ay, Ax) == 0)
                 if (GetSTile (Ay + 24, Ax) < 240)
                    if (GetSTile (Ay, Ax + 16) < 240)
                       if (GetSTile (Ay + 24, Ax + 16) < 240)
                          LetChange = 1;

          if (!LetTrans)
              if (GetSTile (Ay, Ax) == 0)
                  LetTrans = 1;

          sp_MoveSprAbs(Astronaut.Sprite, GameField, Astronaut.CurFrame - Astronaut.PrevFrame,
                        Astronaut.y >> (SENS + 3), Astronaut.x >> (SENS + 3), (Astronaut.x >> SENS) & 7, (Astronaut.y >> SENS) & 7);
          Astronaut.PrevFrame = Astronaut.CurFrame;

          if (Time == 1000 && ColorTime == INK_YELLOW | BRIGHT) { // Time attack
             ColorTime = INK_YELLOW | PAPER_RED | BRIGHT | FLASH;
             wyz_stop_sound ();
             wyz_play_music (3);
             }

          ActParticles();
          ActFire();
          sp_UpdateNow();

        }  // End of game cycle

     wyz_stop_sound();

     if (GameIsOver == 2)
        GameFinished ();
       else
        GameOver ();
}

void PreMenu ()
{
    #asm
;    di
    ld b, 4
    call SETRAMBANK
    ld hl, GFXMENU
    ld de, 16384
    call DEEXO
;    ld b, 0
    call SetRAMBack
;    ei
    #endasm
}


void PostGame ()
{
    #asm

    call _PreMenu

;    di
    ld b, 3
    call SETRAMBANK

    ld de, (_Score)
    call CHECKHiScore
    push af

;    ld b, 0
    call SetRAMBack
;    ei

    pop af
    ret nc

    ld de, (_Score)

;    di
    ld b, 3
    call SETRAMBANK

    call NEW_HISCORE

    HISCORE_CYCLE:

    cp $21 ; ENTER pressed
    jp z, EXIT_HSC

    push af
    push hl

;    ld b, 0
    call SetRAMBack
;    ei

    call FRAME_FX

;    di
    ld b, 3
    call SETRAMBANK

    pop hl
    pop af

    call ER_Hook

    jp HISCORE_CYCLE

    EXIT_HSC:

;    ld b, 0
    call SetRAMBack
;    ei

     #endasm

//    wyz_stop_sound();

}

void CheckFireIsPressed ()
{
     if ((((JoyFunc) (&Keys))) != 127)
        return;

    #asm
        pop af                ; Empty stack from previous "call" returning point
        call FADE_OUT
        call _wyz_stop_sound
        ret
    #endasm

}

void Menu()
{
    #asm

    ld hl, 22528 + 5 * 32 + 5
    ld de, 22528 + 7 * 32 + 5
    ld bc, 21
    ldir
    ld hl, 22528 + 5 * 32 + 5
    ld de, 22528 + 10 * 32 + 5
    ld bc, 21
    ldir
    ld hl, 22528 + 5 * 32 + 5
    ld de, 22528 + 12 * 32 + 5
    ld bc, 21
    ldir


;    di
    ld b, 3
    call SETRAMBANK
    call MENU_ShowMenu

;    ld b, 0
    call SetRAMBack
;    ei

    xor a
    ld (_pi), a
    ld (_ka), a

    MENU_CYCLE:

    call FRAME_FX

    ld a, (_pi)
    and a
    call z, MENU_ATTRCYCLE

    ld a, (_ka)
    inc a
    ld (_ka), a
    cp 75
    call z, MENU_SWITCH

    halt

    #endasm

    JoyFunc = sp_JoyKempston;
    CheckFireIsPressed ();

    Keys.fire  = 383; // sp_LookupKey(' ');
    JoyFunc = sp_JoyKeyboard;
    CheckFireIsPressed ();

    Keys.fire  = 1151; // sp_LookupKey('m');
    CheckFireIsPressed ();

    JoyFunc = sp_JoyKempston;
    CheckFireIsPressed ();

    JoyFunc = sp_JoySinclair1;
    CheckFireIsPressed ();

    JoyFunc = sp_JoySinclair2;
    CheckFireIsPressed ();

    #asm

    JP MENU_CYCLE

MENU_SWITCH:

    xor a
    ld (_ka), a

    ld a, (_pi)
    xor 1
    ld (_pi), a

    jp z, MENU_SMenu

;    di
    ld b, 3
    call SETRAMBANK
    call MENU_ShowHOF
;    ld b, 0
    call SetRAMBack
;    ei

    ret

MENU_SMenu:
;    di
    ld b, 3
    call SETRAMBANK
    call MENU_ShowMenu
;    ld b, 0
    call SetRAMBack
;    ei
    ret

    #endasm
}

void main ()
{
    #asm
    call _Init

    TheNeverEndingStory:

    call _Menu
    call _InitGame
    call _Gamecycle
    call _PostGame

    jp TheNeverEndingStory
    #endasm
}
