#include "speccychar.h"SpeccyChar::SpeccyChar(){    for (int n = 0; n < 8; n++)        Bytes[n] = 0;}SpeccyChar::~SpeccyChar(){}void SpeccyChar::SetByte (int ByteN, unsigned char Byte)
{
    Bytes[ByteN] = Byte;
}

unsigned char SpeccyChar::GetByte (int ByteN)
{
    return Bytes[ByteN];
}

int SpeccyChar::Compare (SpeccyChar Other){    for (int n = 0; n < 8; n++)        if (Bytes[n] != Other.Bytes[n])            return 0;    return 1;/*    if (Bytes[0] == Other.Bytes[0] &&        Bytes[1] == Other.Bytes[1] &&        Bytes[2] == Other.Bytes[2] &&        Bytes[3] == Other.Bytes[3] &&        Bytes[4] == Other.Bytes[4] &&        Bytes[5] == Other.Bytes[5] &&        Bytes[6] == Other.Bytes[6] &&        Bytes[7] == Other.Bytes[7])            return 1;     for (int n = 0; n < 8; n++)        if (~(Bytes[n]) != Other->Bytes[n])            return 0;    // Uno de los dos se tiene que invertir    return 1;*/}void SpeccyChar::Copy (SpeccyChar Other){    for (int n = 0; n < 8; n++)        Bytes [n] = Other.Bytes[n];}void SpeccyChar::Invert(){    for (int n = 0; n < 8; n++)        Bytes[n] = ~Bytes[n];}

void SpeccyChar::Print()
{
    string STR;
    for (int byte = 0; byte < 8; byte++) {
        for (int bit = 7; bit >= 0; bit--)
            if (Bytes[byte] & (1 << bit))
                STR += "##";
             else
                STR += "  ";
        printf ("%i: %s (%i)\n", byte, STR.c_str() , Bytes[byte]);
        STR = "";
    }
    cout << endl;
}

void SpeccyChar::Print (int Byte)
{
    string STR;
    for (int bit = 7; bit >= 0; bit--)
        if (Bytes[Byte] & (1 << bit))
            STR += "##";
          else
            STR += "  ";
    cout << STR;
}
