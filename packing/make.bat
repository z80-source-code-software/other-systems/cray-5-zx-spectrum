del *.tzx
set path=%path%;..\..\z88dk\bin;..\..\Pasmo;..\..\Compresores;..\..\utoloader0.4\bin
set Z80_OZFILES=..\..\z88dk\Lib\
set ZCCCFG=..\..\z88dk\Lib\Config\
buildtzx -l 1 -i template.txt -o Cray-5_Rom.tzx -n Cray-5
buildtzx -l 2 -i template.txt -o Cray-5.tzx -n Cray-5
buildtzx -l 3 -i template.txt -o Cray-5_turbo.tzx -n Cray-5
