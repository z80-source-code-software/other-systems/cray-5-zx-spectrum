org 58952

; Prints the line given by reg A of the charecter given by reg B, on the bottom scan's column given by reg C

PRINTCHR_LINE:

     ld hl, 22496 ; bottom scan
     ld d, 0
     ld e, c
     add hl, de   ; hl points to the place where we must put the A line of B character

     push hl
     ld h, 0
     ld l, b
     add hl, hl
     add hl, hl
     add hl, hl
     ld de, CHARSET
     dec d


     add hl, de   ; hl points to B character

     ld d, 0
     ld e, a
     add hl, de   ; hl points to A line of B character

     pop de       ; de points to where we must draw it

     ld a, (hl)
     ld (de), a

     ret


; Prints the string with HL number from the line given by reg A at bottom scan

PRINTSTR_LINE:

     ex de, hl
     ld hl, TXT_TABLE
     add hl, de
     add hl, de         ; HL points to the string wanted
     ld e, (hl)
     inc hl
     ld d, (hl)
     ex de, hl

     ex af, af'

     ld a, h
     and l
     scf
     ret z

     ex af, af'

     ld c, 0       ; Starts at column 0

     STR_LINE_Hook:

     ex af, af'    ; Keep A

     ld a, (hl)    ; Get current char
     and a
     ret z         ; Return if it is 0 (phrase end)

     ld b, a       ; Pass it to B

     ex af, af'    ; Get back A (line that we must paint)

     push hl
     push af

     call PRINTCHR_LINE

     pop af

     pop hl
     inc hl
     inc c

     jp STR_LINE_Hook ; Repeat again

; ATT_LOCATE: De vuelve en HL la direcci�n de atributos
; Entrada: BC Coordenadas: B = L�nea C = Columna
; Salida: HL Direcci�n de atributos
; Se conservan DE y BC

ATT_LOCATE:
    ld a, b
    sra a
    sra a
    sra a
    add a, #58
    ld h, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld l, a
    ret


; DF_LOCATE: De vuelve en HL la direcci�n del archivo de pantalla
; Entrada: BC Coordenadas: B = L�nea C = Columna
; Salida: HL Direcci�n del archivo de pantalla
; Se conservan DE y BC

DF_LOCATE:
    ld a, b
    and #f8
    add a, #40
    ld h, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld l, a
    ret

; DRAWGFX: Dibuja un gr�fico en pantalla (Tiene que estar por l�neas y en zigzag)
; ENTRADA: HL puntero al gr�fico, BC Coordenadas en caracteres, DE: D = Caract Ancho E = Caract. Alto

DRAWGFX:
     ld (DRGVAR1), bc
     ld (DRGVAR2), bc
     ld (DRGVAR3), de

     push de

     push hl

     DRAWGFX_B0:

     call DF_LOCATE
     ex de, hl
     pop hl                            ; Ahora tenemos en HL el origen y en DE el destino

     ld a, (DRGVAR3 + 1)
     ld b, a
     ld c, 4

     DRAWGFX_B1:

     ld a, (hl)
     ld (de), a
     inc hl
     inc de
     djnz DRAWGFX_B1

     ld a, (DRGVAR3 + 1)
     ld b, a
     dec de
     inc d

     DRAWGFX_B2:

     ld a, (hl)
     ld (de), a
     inc hl
     dec de
     djnz DRAWGFX_B2

     ld a, (DRGVAR3 + 1)
     ld b, a
     inc de
     inc d

     dec c
     jp nz, DRAWGFX_B1


     ld bc, (DRGVAR1)
     inc b
     ld (DRGVAR1), bc


     pop bc               ; Y en BC las dimensiones
     dec c
     push bc
     push hl
     ld bc, (DRGVAR1)
     jp nz, DRAWGFX_B0

     ld bc, (DRGVAR2)


     pop hl

     pop af                 ; Le debemos una a la pila :P
     
     ret


; PRINT: Imprime el car�cter apuntado por DE en las coordenadas BC
; BC: B = Linea, C = Columna
; DE = Direcci�n car�cter
; Como llama a LOCATE y ATT_LOCATE, destruye el contenido de HL
; Salida: DE = Apuntando al primer byte DESPU�S del car�cter actual

PRINT:
      call DF_LOCATE
      ld a, 8
      PRINT_Bucle:
      ex af, af'
      ld a, (de)
      ld (hl), a
      inc de
      inc h
      ex af, af'
      dec a
      jr nz, PRINT_Bucle

      call ATT_LOCATE
      ld de, ATTR_ACT
      ld a, (de)
      ld (hl), a

      ret

; PRINTCHR: Imprime el car�cter cuyo c�digo est� en A en las coordenadas BC
; Entrada: A = Caracter a imprimir
; Salida: DE = Direcci�n donde est� el car�cter

PRINTCHR:
      ld h, 0
      ld l, a
      add hl, hl
      add hl, hl
      add hl, hl
      ld de, CHARSET
      dec d
      add hl, de
      ex de, hl

      call PRINT
      ret

; PRINTSTR: Imprime la cadena apuntada por HL a partir de las coordenadas BC
; ENTRADA:
; BC: B = Fila C = Columna
; HL: Direcci�n donde empieza el texto a imprimir
; Utiliza A
; SALIDA:
; BC = Coordenadas del �ltimo car�cter
; HL = Direcci�n siguiente al �ltimo car�cter (0)
; NO conserva HL

PRINTSTR:
      ld a, (hl)
      and a
      ret z

      push hl
      call PRINTCHR
      pop hl

      inc hl
      inc bc
      ld a, c
      cp 32
      jp c, PRINTSTR
      ld c,0
      inc b
      jp PRINTSTR

MENU_ShowHOF:

     ld hl, 22528 + 7 * 32 + 6
     ld de, 22528 + 7 * 32 + 7
     ld bc, 5
     ld (hl), 0
     ldir

     ld hl, 22528 + 7 * 32 + 6
     ld de, 22528 + 8 * 32 + 6
     ld c, 4
     ldir


     ld a, 6 | 64
     ld (ATTR_ACT), a

     ld hl, TXT_Halloffame
     ld bc, $060a
     call PRINTSTR

     ld a, 7 | 64
     ld (ATTR_ACT), a

     ld hl, STR_HSNAMES
     ld bc, $0906
     call PRINTSTR

     inc hl
     ld bc, $0b06
     call PRINTSTR

     inc hl
     ld bc, $0d06
     call PRINTSTR

     inc hl
     ld bc, $0f06
     call PRINTSTR

     inc hl
     ld bc, $1106
     call PRINTSTR

     ld hl, STR_HISCORE
     ld bc, $0914
     call PRINTSTR
     ld hl, STR_HISCORE
     ld bc, $0b14
     call PRINTSTR
     ld hl, STR_HISCORE
     ld bc, $0d14
     call PRINTSTR
     ld hl, STR_HISCORE
     ld bc, $0f14
     call PRINTSTR
     ld hl, STR_HISCORE
     ld bc, $1114
     call PRINTSTR

     ld de, (HISCORES)
     ld bc, $0919
     call PRINTNUM

     ld de, (HISCORES + 2)
     ld bc, $0b19
     call PRINTNUM

     ld de, (HISCORES + 4)
     ld bc, $0d19
     call PRINTNUM

     ld de, (HISCORES + 6)
     ld bc, $0f19
     call PRINTNUM

     ld de, (HISCORES + 8)
     ld bc, $1119
     call PRINTNUM

     ret


MENU_ShowMenu:


     ld hl, 22528 + 5 * 32 + 4
     ld de, 22528 + 6 * 32 + 4
     ld bc, 24
     ldir

     ld hl, 22528 + 5 * 32 + 4
     ld de, 22528 + 9 * 32 + 4
     ld bc, 24
     ldir

     ld hl, 22528 + 5 * 32 + 4
     ld de, 22528 + 11 * 32 + 4
     ld bc, 24
     ldir

     ld hl, 22528 + 5 * 32 + 4
     ld de, 22528 + 13 * 32 + 4
     ld bc, 24
     ldir

     ld hl, 22528 + 5 * 32 + 4
     ld de, 22528 + 15 * 32 + 4
     ld bc, 24
     ldir

     ld hl, 22528 + 5 * 32 + 4
     ld de, 22528 + 17 * 32 + 4
     ld bc, 24
     ldir

     ld hl, MENU_BCKGRD
     ld bc, $0504
     ld de, $180f
     call DRAWGFX

     xor a
     ld (ATTR_ACT), a

     ld bc, $0706
     ld hl, TXT_PRESS
     call PRINTSTR

     ld de, 22528 + 7 * 32 + 4

     ld hl, ATTR_PRESS
     ld bc, 9
     ldir

     ld bc, $0806
     ld hl, TXT_FIRE
     call PRINTSTR
     ld de, 22528 + 8 * 32 + 4
     ld hl, ATTR_FIRE
     ld bc, 9
     ldir

     ret

CHECKHiScore:

     ld hl, HISCORES - 1

     exx             ; Ponemos registros alternativos
     ld bc, #0500    ; B' = Contador: Son 5 R�cords C' = Contador "invertido"

     PG_B0:

     exx             ; Ponemos registros "normales"

     inc hl
     ld c, (hl)
     inc hl
     ld b, (hl)

     call CP16
     
     ret c

     exx             ; POnemos registros alternativos

     inc c           ; Aqu� (en C') vamos a llevar el n�mero que vamos a ocupar en la tabla
     djnz PG_B0      ; Porque el contador del bucle lo tenemos en B'

     exx             ; Restauramos registros "normales"

     and a

     ret

ACTUALIZE_HISCORES:      ; DE lleva la puntuaci�n conseguida
     push de

     ld hl, HISCORES + 7
     ld de, HISCORES + 9

     exx
     ld a, 4
     sub c          ; en c' est� el n�mero que vamos a ocupar en la tabla de r�cords - 1

     sla a          ; Lo multiplicamos por dos, porque son dos bytes por cada puntuaci�n

     ld e, a
     ld d, 0

     push de
     exx

     pop bc

     ld a, b
     or c
     jp z, AR_NoLDDR

     lddr                 ; Primero desplazamos los r�cords batidos hacia abajo

     AR_NoLDDR:

     inc hl              ; Ahora vamos a "inyectar" el r�cord nuevo
     pop de              ; DE lleva la puntuaci�n conseguida

     ld (hl), e
     inc hl
     ld (hl), d

     ; Ahora vamos a por los nombres

     ld hl, STR_HISCORE - 9
     ld de, STR_HISCORE - 1

     exx

     ld a, 4
     sub c   ; en c' est� el n�mero que vamos a ocupar en la tabla de r�cords - 1

     sla a
     sla a
     sla a  ; x8

     ld e, a
     ld d, 0

     push de

     exx

     pop bc

     ld a, b
     or c
     ret z

     lddr

     ret

NEW_HISCORE:
     call ACTUALIZE_HISCORES

     inc hl
     push hl

     ld d, h
     ld e, l
     inc e

     ld (hl), ' '
     ld bc, 6
     ldir

     ld hl, MENU_BCKGRD
     ld bc, $0504
     ld de, $180f
     call DRAWGFX


     ld a, 6 | 64
     ld (ATTR_ACT), a

     ld hl, TXT_HiScore1
     ld bc, $0708
     call PRINTSTR

     ld a, 5 | 64
     ld (ATTR_ACT), a

     ld hl, TXT_HiScore2
     ld bc, $0906
     call PRINTSTR
     ld hl, TXT_HiScore3
     ld bc, $0a08
     call PRINTSTR

     ld a, 7 | 64
     ld (ATTR_ACT), a

     ld hl, TXT_HiScore4
     ld bc, $0c08
     call PRINTSTR

     ld hl, DRGVAR2                          ; La vamos a usar como contador de caracteres
     ld (hl), 0

     pop hl
     ld (DRGVAR3), hl

     ld hl, TXT_Blank
     ld bc, $0f07
     call PRINTSTR

     ENTERRECORD_Bucle:

     call KFIND

     ret

     ER_Hook:

     push hl
     push af
     call ER_SHOWCURSOR
     pop af
     pop hl

     cp #ff
     jp z, ENTERRECORD_NoKey

     cp 40
     jp z, ENTERRECORD_Delete

;     cp KEY_EN
;     ret z

     push hl
     ld hl, PUEDEDISPARAR
     ld a, (hl)
     and a
     pop hl
     jp z, ENTERRECORD_Bucle

     call ENTERRECORD_Key

     jp ENTERRECORD_Bucle

ENTERRECORD_NoKey:
     ld hl, PUEDEDISPARAR
     ld (hl), 1
     jp ENTERRECORD_Bucle

ENTERRECORD_Key:
     push hl

     ld hl, DRGVAR2
     ld a, (hl)
     cp 7

     pop hl

     ret z

     ld a, (hl)     ; Recuperamos tecla pulsada

     cp '0'
     ret c

     cp 'Z'+1
     ret nc

     ld hl, DRGVAR2
     ld e, (hl)
     ld d, 0
     inc (hl)


     ld hl, (DRGVAR3)
     add hl, de
     ld (hl), a

     call ER_SHOWSTR

     ld hl, PUEDEDISPARAR
     ld (hl), 0


     ret

ENTERRECORD_Delete:
     ld hl, PUEDEDISPARAR
     ld a, (hl)
     and a
     jp z, ENTERRECORD_Bucle


     ld hl, DRGVAR2
     ld a, (hl)
     and a
     jp z, ENTERRECORD_Bucle

     dec (hl)
     ld e, (hl)
     ld d, 0
     ld hl, (DRGVAR3)
     add hl, de
     ld (hl), ' '

     call ER_SHOWSTR

     ld hl, PUEDEDISPARAR
     ld (hl), 0

     jp ENTERRECORD_Bucle

ER_SHOWSTR:
     ld hl, (DRGVAR3)
     ld bc, #0f0D
     call PRINTSTR

ER_SHOWCURSOR:
     ld bc, $0f0d
     ld a, (DRGVAR2)
     add a, c
     ld c, a
     ld hl, STR_CURSOR
     call PRINTSTR

     ld hl, STR_CURSOR
     ld a, (hl)
     ld (hl), ' '
     cp ' '
     ret nz
     ld (hl), '_'
     ret


; PRINTNUM: Imprime el n�mero dado por DE en las coordenadas BC
; Utiliza A
PRINTNUM:
      ld (DRGVAR1), de
      ld a, 10
      ld (DRGVAR2), a

      push bc
      call DIVI
      pop bc

      add a, '0'

      call PRINTCHR

      dec c

      ld de, (DRGVAR1)

      ld a, d
      or e
      jp nz, PRINTNUM

      ret

; DIVI: Divide VAR1 entre VAR2, y pone el resultado en VAR1 y el resto en el VAR2
; Utiliza: HL, BC, DE, AF

DIVI:
     ld hl, DRGVAR2
     ld c, (hl)

     ld b, 0
     ld de, -1
     ld hl, (DRGVAR1)
     or a
DIVI_Bucle:
     inc de
     sbc hl, bc
     jp nc, DIVI_Bucle

DIVI_Final:
     add hl, bc
     ld a, l
;     ld (DRGVAR2), a
     ld (DRGVAR1), de
     ret

;CP16: Compara BC con DE

CP16:
     ld a, b
     cp d
     ret nz
     ld a, c
     cp e
     ret


KFIND:
     call KFIND1
     ld a, d
     cp #ff
     ret z

     ld e, d
     ld d, 0
     ld hl, STR_KEYREAD
     add hl, de
     ret

KFIND1:
     ld de, #ff2f
     ld bc, #fefe

NXHALF:
     in a, (c)
     cpl
     and #1f
     jr z, NPRESS

     inc d
     ret nz

     ld h,a
     ld a,e
KLOOP:
     sub 8
     srl h
     jr nc, KLOOP

     ret nz

     ld d,a

NPRESS:
     dec e
     rlc b
     jr c, NXHALF

     cp a
     ret z

STR_KEYREAD:

DEFB 'BHY65TGVNJU74RFCMKI83EDX','Z'+5,'LO92WSZ','Z'+6,'Z'+7,'P01QA', 'Z'+4

ATTR_ACT:
      defb 0
DRGVAR1:
      defw 0
DRGVAR2:
      defw 0
DRGVAR3:
      defw 0
PUEDEDISPARAR:
      defb 0

CHARSET:
include "../gfx/charset.ASM"

MENU_BCKGRD:
include "../gfx/menu_background.ASM"

TXT0:
     DEFB '  Congratulations!!!', 0
TXT1:
     DEFB ' You have managed to',0
TXT2:
     DEFB 'save the space colony',0
TXT3:
     DEFB ' from a catastrophic ',0
TXT4:
     DEFB '      ending...  ',0
TXT5:
     DEFB '  CRAY - 5 SPECTRUM',0
TXT6:
     DEFB 127,' 2011  by RetroWorks',0
TXT7:
     DEFB '         CREW',0
TXT8:
     DEFB '        CODING',0
TXT9:
     DEFB '        Benway',0
TXT10:
     DEFB '     GRAPHIC  ART',0
TXT11:
     DEFB '     Pangantipaco',0
TXT12:
     DEFB '     SOUND  CODE',0
TXT13:
     DEFB '         WYZ',0
TXT14:
     DEFB '        MUSIC',0
TXT15:
     DEFB '      Iggy Rock',0
TXT16:
     DEFB '       TESTING',0
TXT17:
     DEFB '   RetroWorks Team',0
TXT18:
     DEFB '   Dedicated to...',0
TXT19:
     DEFB 'Miguel  Garcia Rengel',0
TXT20:
     DEFB '   Wyzinho Junior  ',0
TXT21:
     DEFB 'Check out our webpage',0
TXT22:
     DEFB ' to enjoy  new games',0
TXT23:
     DEFB ' for 8-bit computers',0
TXT24:
     DEFB '  www.retroworks.es',0
TXT25:
     DEFB '  Based on ', 34
     DEFB 'Cray 5',34,0
TXT26:
     DEFB ' Published  by Topo', 0
TXT27:
     DEFB 'in 1987, only for CPC',0
TXT28:
     DEFB '  and developed by',0
TXT29:
     DEFB 'Salvador Casamiquela',0
TXT30:
     DEFB '   Cinto  Ventura',0
TXT31:
     DEFB '     Gominolas',0

TXT_Blank:
     DEFS 20, ' '
     DEFB 0

TXT_TABLE:
     DEFW TXT0, TXT_Blank, TXT1, TXT2, TXT3, TXT4, TXT_Blank
     DEFW TXT5, TXT6, TXT_Blank, TXT_Blank

     DEFW TXT25, TXT26, TXT27, TXT28, TXT29, TXT30, TXT31, TXT_Blank, TXT_Blank

     DEFW TXT7, TXT_Blank, TXT8,TXT9,TXT_Blank
     DEFW TXT10,TXT11,TXT_Blank
     DEFW TXT12,TXT13,TXT_Blank
     DEFW TXT14,TXT15,TXT_Blank
     DEFW TXT16,TXT17,TXT_Blank
     DEFW TXT18,TXT19,TXT20,TXT_Blank
     DEFW TXT21,TXT22,TXT23,TXT24,TXT_Blank

     DEFW TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank
     DEFW TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank
     DEFW TXT_Blank,TXT_Blank,TXT_Blank,TXT_Blank
     DEFW 0

TXT_Halloffame:
     defb 'HALL OF FAME', 0
STR_HSNAMES:
        DEFB 'RETRO  ',0
        DEFB '  WORKS',0
        DEFB 'RETRO  ',0
        DEFB '  WORKS',0
        DEFB 'CRAY  5',0
STR_HISCORE:                  ; No mover
        DEFB '00000',0

STR_CURSOR:
        defb '_ ',0


HISCORES:
    DEFW 500, 400, 300, 200, 100

TXT_PRESS:
    defb 'PRESS',0
TXT_FIRE:
    defb 'FIRE',0
ATTR_PRESS:
    defb 7, 6 | 64, 7 | 64, 6 | 64, 7, 6, 4, 4, 6
ATTR_FIRE
    defb 7, 6, 4, 4, 6, 7, 6 | 64, 7 | 64, 6 | 64

TXT_HiScore1:
    defb 'CONGRATULATIONS!', 0
TXT_HiScore2:
    defb 'YOU DESERVE  A PLACE', 0
TXT_HiScore3:
    defb 'AMONG  THE ELITE', 0
TXT_HiScore4:
    defb 'ENTER YOUR  NAME', 0


END 58952
