#include "tileextract.h"

TileExtract::TileExtract()
{
}

TileExtract::~TileExtract()
{
    if (Screens != NULL) {
        delete [] Screens;
        delete [] STOnScreen;
    }
    if (SuperTiles != NULL) {
        for (int n = 0; n < 256; n++)
            delete SuperTiles[n];
        delete [] SuperTiles;
    }
}

int TileExtract::Compare (const string &c, const string &c2)
{
    string::const_iterator p = c.begin();
    string::const_iterator p2 = c2.begin();

    while (p != c.end() && p2 != c.end()) {
        if (toupper(*p) != toupper(*p2))
            return (toupper (*p) < toupper (*p2))?-1:1;
        ++p;
        ++p2;
    };

    return (c2.size() == c.size()) ? 0 : (c.size() < c2.size()) ? -1 : 1;
}

int TileExtract::AskForHelp ()
{
    for (int n = 0; n < P->Counter() ; n++)
         if (!Compare (P->Arg(n), "--h"))
             return 1;

    for (int n = 0; n < P->Counter() ; n++)
         if (!Compare (P->Arg(n), "moo") || !Compare (P->Arg(n), "-moo"))
            return 2;

    return 0;
}

int TileExtract::Validate ()
{
    int InputPar, OutputPar, WidthPar, HeightPar, TypePar, AttrPar;

    InputPar = P->SearchString ("-i");
    OutputPar = P->SearchString ("-o");
    WidthPar = P->SearchString ("-w");
    HeightPar = P->SearchString ("-h");
    TypePar = P->SearchString ("-t");
    AttrPar = P->SearchString ("-a");


    if (InputPar < 0 || InputPar + 1 == P->Counter()) {
        cout << "Error! You must specificate a input file at least!!!\n\n";
        cout << "Type TileExtract --h for help.\n\n";
        return 0;
    }

    InputName = P->Arg (InputPar + 1);
    if (InputName.find (".scr") != string::npos)
        InputName.replace (InputName.find (".scr"), 4, "");

    if (OutputPar < 0)
        OutputName = InputName;
      else
        if (OutputPar +1 == P->Counter() ||
            Compare (P->Arg (OutputPar + 1), "-i") == 0 ||
            Compare (P->Arg (OutputPar + 1), "-w") == 0 ||
            Compare (P->Arg (OutputPar + 1), "-h") == 0 ) {
            cout << "Error! You have specificate -o parameter to set a output filename, but you forget the name!!!\n";
            cout << "Using " << InputName << " as output file.\n";
            OutputName = InputName;
            }
        else {
            cout << Compare (P->Arg (OutputPar + 1), "-w");
            OutputName = P->Arg (OutputPar + 1);
            if (OutputName.find (".asm") != string::npos)
                OutputName.replace (OutputName.find (".asm"), 4, "");
            }


    if (WidthPar < 0)
        Width = 2;
     else
        if (WidthPar + 1 < P->Counter() && isdigit (P->Arg(WidthPar + 1) [0]))
            Width = atoi (P->Arg(WidthPar + 1));
         else
            Width = 0;

    if (Width < 2 || Width > 16) {
        cout << "Error! Width value not valid (must be between 2 and 16) !!!\n\n";
        return 0;
    }

    if (HeightPar < 0)
        Height = 2;
     else
        if (HeightPar + 1 < P->Counter() && isdigit (P->Arg(HeightPar + 1) [0]))
            Height = atoi (P->Arg(HeightPar + 1));
         else
            Height = 0;

    if (Height < 2 || Height > 16) {
        cout << "Error! Height value not valid (must be between 2 and 16) !!!\n\n";
        return 0;
    }

    if (TypePar < 0)
        OutputType = 0;
     else
        if (TypePar + 1 < P->Counter() && isdigit (P->Arg(TypePar + 1) [0]))
            OutputType = atoi (P->Arg(TypePar + 1));
         else
            OutputType = -1;

    if (OutputType < 0 || OutputType > 2) {
        cout << "Error! Output type not valid (must be between 0 and 2)\n\n";
        return 0;
    }

    if (AttrPar < 0)
        AttrType = 0;
     else
        if (AttrPar + 1 < P->Counter() && isdigit (P->Arg(AttrPar + 1) [0]))
            AttrType = atoi (P->Arg(AttrPar + 1));
         else
            AttrType = -1;

    if (AttrType < 0 || AttrType > 3) {
        cout << "Error! ATTR output mode not valid (must be between 0 and 2)\n\n";
        return 0;
    }


    return 1;
}

int TileExtract::OpenFiles()
{
    ifstream *File;
    string Name[100];
    char Str[100];

    NumberOfFiles = 0;

    Name[0] = InputName + ".scr";
    cout << "Looking for " << Name[0] << "... ";
    File = new ifstream (Name[0].c_str(), ios::binary | ios::in);
    if (File->fail()) {
        delete File;
        cout << "Not found!\n";
        Name[0] = InputName + "_01.scr";
        cout << "Looking for " << Name[0] << "... ";
        File = new ifstream (Name[0].c_str(), ios::binary | ios::in);
        if (File->fail()) {
            delete File;
            cout << "Not found!\n";
            cout << "ERROR! File " << InputName << ".scr not found!!!\n\n";
            return 1;
            }
        while (!File->fail()) {
            cout << "Found!\n";
            delete File;
            if ((++NumberOfFiles) + 1 < 10)
                sprintf (Str, "%s_0%i.scr", InputName.c_str(), NumberOfFiles + 1);
             else
                sprintf (Str, "%s_%i.scr", InputName.c_str(), NumberOfFiles + 1);
            Name[NumberOfFiles] = Str;
            cout << "Looking for " << Name[NumberOfFiles] << "... ";
            File = new ifstream (Name[NumberOfFiles].c_str(), ios::binary | ios::in);
        }
        cout << "Not found!\n";
    }
    else {
        cout << "Found!\n";
        if (NumberOfFiles == 0)
            NumberOfFiles = 1;
    }
    delete File;

    Screens = new SpeccyBMP [NumberOfFiles];
    STOnScreen = new char [NumberOfFiles];
    for (int SN = 0; SN < NumberOfFiles; SN++) {
        Screens[SN].Load (Name[SN]);
        STOnScreen[SN] = 0;
    }

    cout << NumberOfFiles << " File(s) found(s).\n\n";

    return 0;
}

int TileExtract::ExistsSTile (SuperTile &ST)
{
    for (int n = 0; n < NumberOfSTiles; n++)
        if (SuperTiles[n]->Compare (ST))
            return n;
    return -1;
}

int TileExtract::InsertSTile (SuperTile &ST)
{
    SuperTiles[NumberOfSTiles++]->Copy (ST);
//    cout << NumberOfSTiles << ":\n";
//    ST.Print (TileSet);
    return NumberOfSTiles;
}

int TileExtract::ExistsTile (SpeccyChar T)
{
    for (int n = 0; n < NumberOfTiles; n++)
        if (TileSet[n].Compare (T))
            return n;
    return -1;
}

int TileExtract::InsertTile (SpeccyChar T)
{
    TileSet[NumberOfTiles++].Copy (T);
//    cout << NumberOfTiles << ":\n";
//    T.Print();
    return NumberOfTiles;
}

int TileExtract::ExtractTiles()
{
    for (int ScreenN = 0; ScreenN < NumberOfFiles; ScreenN++)
        for (int y = 0; y < 24; y++)
            for (int x = 0; x < 32; x++)
                if (ExistsTile (Screens[ScreenN].GetChar (x, y)) < 0)
                    if (InsertTile (Screens[ScreenN].GetChar (x, y)) > 255)
                        return 256;
    return NumberOfTiles;
}

int TileExtract::ExtractSTiles()
{
    SuperTile STE (Width, Height);
    int TileN;

    for (int ScreenN = 0; ScreenN < NumberOfFiles; ScreenN++)
        for (int y = 0; y < 24 - (24 % Height); y += Height)
            for (int x = 0; x < 32 - (32 % Width); x += Width) {
                for (int h = 0; h < Height; h++)
                    for (int w = 0; w < Width; w++) {
                        TileN = ExistsTile (Screens[ScreenN].GetChar (x + w, y + h));
                        STE.SetTile (w, h, TileN);
                        STE.SetAttr (w, h, Screens[ScreenN].GetAttr (x + w, y + h));
                    }
                if (ExistsSTile (STE) < 0) {
                    if (InsertSTile (STE) > 255)
                        return 256;
                    STOnScreen[ScreenN]++;
                }
            }

    return 0;
}

int TileExtract::OutputASM ()
{
    ofstream *File;
    string Name;
    char Str[100];
    int STileIndex = 0;
    int CST = 0;

    Name = OutputName + "_tileset.asm";

    File = new ofstream (Name.c_str(), ios::out);
    if (!File) {
        cout << "Error opening file " << Name << " for writting!!!\n";
        return 1;
    }
    *File << "; Tileset definition generated by TileExtract\n\n";

    for (int n = 0; n < NumberOfTiles; n++) {
        *File << "Tile_" << n << ":\n";
        *File << "defb";
        for (int byte = 0; byte < 8; byte++) {
            sprintf (Str, "%i", TileSet[n].GetByte(byte));
            *File << " " << Str;
            if (byte < 7)
                *File << ",";
            }
        *File << endl << endl;
    }
    File->flush();

    delete File;

    cout << "Tileset extracted to " << Name << " succesfully!\n";

    for (int FNum = 1; FNum <= NumberOfFiles; FNum++) {
        if (FNum < 10)
            sprintf (Str, "%s_0%i.asm", OutputName.c_str(), FNum);
          else
            sprintf (Str, "%s_%i.asm", OutputName.c_str(), FNum);

        Name = Str;

        File = new ofstream (Name.c_str(), ios::out);
        if (File->fail()) {
            cout << "Error opening file " << Name << " for writting!!!\n";
            return 2;
        }
        *File << "; SuperTile definition generated by TileExtract\n\n";

        for (int n = 0; n < STOnScreen [FNum - 1]; n++, STileIndex++) {
            *File << "SuperTile_" << STileIndex << ":\n";
            *File << "defb";

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++) {
                    sprintf (Str, "%i", SuperTiles[STileIndex]->GetTile (x, y));
                    *File << " " << Str;
                    if (AttrType == 2) {
                        *File << ", ";
                        sprintf (Str, "%i", SuperTiles[STileIndex]->GetAttr (x, y));
                        *File << " " << Str;
                        }
                    if (y + 1 < Height || x + 1 < Width)
                        *File << ",";
                }

            if (AttrType == 0) {
                *File << endl << "SuperTile_" << STileIndex << "_ATTR:\ndefb ";
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++) {
                        sprintf (Str, "%i", SuperTiles[STileIndex]->GetAttr (x, y));
                        *File << " " << Str;
                        if (y + 1 < Height || x + 1 < Width)
                            *File << ",";
                    }
                }
            *File << endl << endl;
            CST = n;
        }

        if (AttrType == 1) {
            STileIndex -= STOnScreen [FNum - 1];
            for (int n = 0; n < STOnScreen [FNum - 1]; n++, STileIndex++) {
                *File << endl << "SuperTile_" << STileIndex << "_ATTR:\ndefb ";
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++) {
                        sprintf (Str, "%i", SuperTiles[STileIndex]->GetAttr (x, y));
                        *File << " " << Str;
                        if (y + 1 < Height || x + 1 < Width)
                            *File << ",";
                    }
                *File << endl;
                }
        }

    cout << CST+1 << " SuperTiles extracted to " << Name << " succesfully!\n";

    File->flush();
    delete File;
    }

    return 0;
}

int TileExtract::OutputC ()
{
    ofstream *File;
    string Name;
    char Str[100];
    int STileIndex = 0;
    int CST = 0;

    Name = OutputName + "_tileset.h";

    File = new ofstream (Name.c_str(), ios::out);
    if (!File) {
        cout << "Error opening file " << Name << " for writting!!!\n";
        return 1;
    }
    *File << "// Tileset definition generated by TileExtract\n\n";

    for (int n = 0; n < NumberOfTiles; n++) {
        *File << "unsigned char Tile" << n << "[8] = {";
        for (int byte = 0; byte < 8; byte++) {
            sprintf (Str, "%i", TileSet[n].GetByte(byte));
            *File << " " << Str;
            if (byte < 7)
                *File << ",";
            }
        *File << "};" << endl;
    }
    File->flush();

    delete File;

    cout << "Tileset extracted to " << Name << " succesfully!\n";

    for (int FNum = 1; FNum <= NumberOfFiles; FNum++) {
        if (FNum < 10)
            sprintf (Str, "%s_0%i.h", OutputName.c_str(), FNum);
          else
            sprintf (Str, "%s_%i.h", OutputName.c_str(), FNum);

        Name = Str;

        File = new ofstream (Name.c_str(), ios::out);
        if (File->fail()) {
            cout << "Error opening file " << Name << " for writting!!!\n";
            return 2;
        }
        *File << "// SuperTile definition generated by TileExtract\n\n";

        for (int n = 0; n < STOnScreen [FNum - 1]; n++, STileIndex++) {
            *File << "unsigned char SuperTile_" << STileIndex << " [";
            sprintf (Str, "%i", Width * Height);
            if (AttrType == 2)
                sprintf (Str, "%i", Width * Height * 2);
            *File << Str << "] = {";

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++) {
                    sprintf (Str, "%i", SuperTiles[STileIndex]->GetTile (x, y));
                    *File << " " << Str;
                    if (AttrType == 2) {
                        *File << ",";
                        sprintf (Str, "%i", SuperTiles[STileIndex]->GetAttr (x, y));
                        *File << " " << Str;
                        }
                    if (y + 1 < Height || x + 1 < Width)
                        *File << ",";
                }
            *File << "};" << endl;

            if (AttrType == 0) {
                *File << "unsigned char SuperTile_" << STileIndex << "_ATTR [";
                sprintf (Str, "%i", Width * Height);
                *File << Str << "] = {";
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++) {
                        sprintf (Str, "%i", SuperTiles[STileIndex]->GetAttr (x, y));
                        *File << " " << Str;
                        if (y + 1 < Height || x + 1 < Width)
                            *File << ",";
                    }
                *File << "};" << endl << endl ;
                }

            CST = n;
        }


        if (AttrType == 1) {
            STileIndex -= STOnScreen [FNum - 1];
            for (int n = 0; n < STOnScreen [FNum - 1]; n++, STileIndex++) {
                *File << endl << "unsigned char SuperTile_" << STileIndex << "_ATTR [";
                sprintf (Str, "%i", Width * Height);
                *File << Str << "] = {";
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++) {
                        sprintf (Str, "%i", SuperTiles[STileIndex]->GetAttr (x, y));
                        *File << " " << Str;
                        if (y + 1 < Height || x + 1 < Width)
                            *File << ",";
                    }
                *File << "};";
                }
        }

        cout << CST+1 << " SuperTiles extracted to " << Name << " succesfully!\n";

        File->flush();
        delete File;
    }

    return 0;
}

int TileExtract::OutputBIN ()
{
    ofstream *File;
    string Name;
    char Str[100];
    int STileIndex = 0;
    int CST = 0;

    Name = OutputName + "_tileset.bin";

    File = new ofstream (Name.c_str(), ios::binary | ios::out);
    if (!File) {
        cout << "Error opening file " << Name << " for writting!!!\n";
        return 1;
    }

    for (int n = 0; n < NumberOfTiles; n++)
        for (int byte = 0; byte < 8; byte++)
            File->put (TileSet[n].GetByte(byte));
    File->flush();

    delete File;

    cout << "Tileset extracted to " << Name << " succesfully!\n";

    for (int FNum = 1; FNum <= NumberOfFiles; FNum++) {
        if (FNum < 10)
            sprintf (Str, "%s_0%i.bin", OutputName.c_str(), FNum);
          else
            sprintf (Str, "%s_%i.bin", OutputName.c_str(), FNum);

        Name = Str;

        File = new ofstream (Name.c_str(), ios::binary | ios::out);
        if (File->fail()) {
            cout << "Error opening file " << Name << " for writting!!!\n";
            return 2;
        }

        for (int n = 0; n < STOnScreen [FNum - 1]; n++, STileIndex++) {
            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++) {
                    File->put (SuperTiles[STileIndex]->GetTile (x, y));
                    if (AttrType == 2)
                        File->put (SuperTiles[STileIndex]->GetAttr (x, y));
                    }

            if (AttrType == 0)
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++)
                        File->put (SuperTiles[STileIndex]->GetAttr (x, y));
            CST = n;
        }


        if (AttrType == 1) {
            STileIndex -= STOnScreen [FNum - 1];
            for (int n = 0; n < STOnScreen [FNum - 1]; n++, STileIndex++)
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++)
                        File->put (SuperTiles[STileIndex]->GetAttr (x, y));
        }

        cout << CST+1 << " SuperTiles extracted to " << Name << " succesfully!\n";

        File->flush();
        delete File;
    }

    return 0;
}


int TileExtract::Run(Parser *parser)
{
    cout << "TileExtract v.1.0\n" << "Tile extractor from Sinclair ZX Spectrum's screen bitmaps\n" << "Coded by Luis I. García a.k.a. Benway (garvelu@yahoo.es)\n";
    cout << "Released under GPLv3 license (http://www.gnu.org/licenses/gpl-3.0.html)\n\n";

    P = parser;

    switch (AskForHelp ()) {
        case 1:
            cout << "Usage:" << endl;
            cout << "TileExtract -i <input file> [-o <ouput file>] [-w <Width>] [-h <Height>] [-t 0|1|2] [-a 0|1|2]\n\n";
            cout << "If no output file name is indicated, same as input will be used, with correspondent extension.\n\n";
            cout << "Both Width and Height are expressed in Spectrum characters (8 x 8 pixels). (DEFAULT = 2)\n\n";
            cout << "-t defines output type:\n";
            cout << "                   0: C header file. (DEFAULT)\n";
            cout << "                   1: Assembler file.\n";
            cout << "                   2: Binary file.\n\n";
            cout << "-a defines attribute data place in file:\n";
            cout << "                   0: After each SuperTile definition. (DEFAULT)\n";
            cout << "                   1: After all SuperTiles on file.\n";
            cout << "                   2: After each Tile on the SuperTiles definition.\n\n\n";
            cout << "You can use file series whenever their names finish as _XX (XX = 01 -> 99), obtaining only one tileset, ";
            cout << "and SuperTiles definition will be written in so many files as it uses for input.\n\n\n";
            cout << "This program also has SuperCow powers.\n";
            cout << endl;
            return 0;
        case 2:
            cout << "         (__) " << endl;
            cout << "         (oo) " << endl;
            cout << "   /------" << '\\' << "/ " << endl;
            cout << "  / |    ||   " << endl;
            cout << " *  /" << '\\' << "---/" << '\\' << endl;
            cout << "    ~~   ~~   " << endl;
            cout << "...." << '"' << "Have you mooed today?"<< '"' << "..." << endl;
            return 0;
    }

    if (!Validate())
        return 1;

    SuperTiles = new SuperTile*[256];
    for (int n = 0; n < 256; n++)
        SuperTiles[n] = new SuperTile (Width, Height);


    if (OpenFiles())
        return 1;

    if (ExtractTiles() > 255) {
        cout << "256 tiles reached!!!\n\n";
        return 1;
    }

    ExtractSTiles();

    printf ("%i Tile(s) found(s). ", NumberOfTiles);
    printf ("%i SuperTile(s) found(s).\n\n", NumberOfSTiles);

    if (NumberOfTiles)
        switch (OutputType) {
            case 0:
                return OutputC();
            case 1:
                return OutputASM();
            case 2:
                return OutputBIN();
        }

    cout << "Error!!! NO TILES FOUND??\n\n";

    return 0;
}
