#ifndef SPECCYCHAR_H#define SPECCYCHAR_H

#include <iostream>using namespace std;class SpeccyChar{    public:        SpeccyChar();        ~SpeccyChar();        void SetByte (int ByteN, unsigned char Byte);
        unsigned char GetByte (int ByteN);
        int Compare (SpeccyChar Other);        void Copy (SpeccyChar Other);
        void Print ();
        void Print (int Byte);    private:        unsigned char Bytes[8];        void Invert ();};#endif
