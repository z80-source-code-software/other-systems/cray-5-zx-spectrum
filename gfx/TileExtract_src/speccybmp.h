#ifndef SPECCYBMP_H#define SPECCYBMP_H#include "speccychar.h"
#include <iostream>
#include <fstream>
#include <string>
#include <ctype.h>
#include <stdlib.h>
using namespace std;class SpeccyBMP{    public:        SpeccyBMP();        ~SpeccyBMP();        int Load(string Name);
        SpeccyChar GetChar (int x, int y);        unsigned char GetAttr (int x, int y);    private:        SpeccyChar Chars[32][24];        unsigned char Attr [32][24];};#endif

