#ifndef SUPERTILE_H#define SUPERTILE_H

#include "speccychar.h"using namespace std;class SuperTile{    public:        SuperTile(int w, int h);        ~SuperTile();        int Compare (SuperTile ST);        void Copy (SuperTile &ST);        void SetTile (int x, int y, unsigned char NTile);        unsigned char GetTile (int x, int y);        void SetAttr (int x, int y, unsigned char NAttr);        unsigned char GetAttr (int x, int y);
        void Print (SpeccyChar *TileSet);
    private:        int Width, Height;        unsigned char **Tile;        unsigned char **Attr;};#endif

