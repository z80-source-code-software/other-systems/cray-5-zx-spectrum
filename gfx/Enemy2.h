/* C source file created by SevenuP v1.20                                */
/* SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain*/

/*
GRAPHIC DATA:
Pixel Size:      ( 24,  24)
Char Size:       (  3,   3)
Frames:             4
Sort Priorities: Mask, Char line, Y char, X char, Frame number
Data Outputted:  Gfx
Interleave:      Frames
Mask:            Yes
*/

unsigned char Enemy2[576] = {
 19,  0, 69,  0,131,  0, 96,  0,
 31,  0,  0,  0,  1,  0,  1,  0,
  1,  0,  1,  0,  1,  0, 21,  0,
 64,  0,131,  0, 96,  0, 31,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
200,  0,162,  0, 65,  0,  6,  0,
248,  0,  0,  0,  0,  0,128,  0,
  0,  0,128,  0,  0,  0,168,  0,
  2,  0,193,  0,  6,  0,248,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  3,  0,  5,  0, 19,  0, 64,  0,
129,  0, 96,  0, 31,  0,  0,  0,
 21,  0, 65,  0,129,  0, 96,  0,
 31,  0,  0,  0,  5,  0,  3,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
192,  0,160,  0, 72,  0,  2,  0,
  1,  0,  6,  0,248,  0,  0,  0,
 40,  0,130,  0,  1,  0,  6,  0,
248,  0,  0,  0,160,  0, 64,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  3,  0,  5,  0,  3,  0,  0,  0,
  1,  0, 21,  0, 65,  0,129,  0,
 96,  0, 31,  0,  0,  0,  1,  0,
  0,  0,  3,  0,  5,  0,  3,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
192,  0,160,  0, 64,  0,  0,  0,
  0,  0,168,  0,  2,  0,129,  0,
  6,  0,248,  0,  0,  0,128,  0,
  0,  0,192,  0,160,  0, 64,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  3,  0,  5,  0, 19,  0, 64,  0,
129,  0, 96,  0, 31,  0,  0,  0,
 21,  0, 65,  0,129,  0, 96,  0,
 31,  0,  0,  0,  5,  0,  3,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
192,  0,160,  0, 72,  0,  2,  0,
  1,  0,  6,  0,248,  0,  0,  0,
 40,  0,130,  0,  1,  0,  6,  0,
248,  0,  0,  0,160,  0, 64,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0};
