
; SPECTRUM PSG proPLAYER - WYZ 2010

; ENSAMBLAR CON AsMSX

; CARACTERISTICAS
; 6 OCTAVAS:            O[2-6]=60 NOTAS
; 4 LONGITUDES DE NOTA: L[0-3]+PUNTILLO 
; PUNTILLO
; COMANDOS:     T:TEMPO
;               I:INSTRUMENTO
;               S:REPRODUCTOR DE EFECTOS CANAL C


; LOS DATOS QUE HAY QUE VARIAR :

; * N� DE CANCION. 
; * TABLA DE CANCIONES



; POR INCLUIR
; - ELEGIR CANAL DE EFECTOS

		.ORG	0xc000
		.START	PLAYER
		;.SINCLAIR
		.FILENAME	"..\bin\wyzplayer"


PLAYER:
;               DI


		CALL	PLAYER_OFF

; MUSICA DATOS INICIALES

		LD	DE,$0010		;  N� BYTES RESERVADOS POR CANAL
                LD      HL,BUFFER_DEC       	;* RESERVAR MEMORIA PARA BUFFER DE SONIDO!!!!!
                LD      [CANAL_A],HL
                
                ADD     HL,DE       	
                LD      [CANAL_B],HL       	

                ADD     HL,DE       	
                LD      [CANAL_C],HL 

                ADD     HL,DE       	
                LD      [CANAL_P],HL
                
                RET

;                LD      A,0             	;* CANCION N� 0
;                CALL    CARGA_CANCION

;PANTALLA



;		EI

;LOOP:		HALT
;		CALL	INICIO
;		JR	LOOP

                


;___________________________________________________________

                DB      "PSG PROPLAYER BY WYZ'10"

;___________________________________________________________

	
;___________________________________________________________

INICIO:		CALL    ROUT
		LD	HL,PSG_REG
		LD	DE,PSG_REG_SEC
		LD	BC,14
		LDIR				
                CALL    REPRODUCE_SONIDO         
                CALL    PLAY
                CALL    REPRODUCE_EFECTO
  		RET


;INICIA EL SONIDO N� [A]

INICIA_SONIDO:  LD      HL,TABLA_SONIDOS
                CALL    EXT_WORD
                LD      [PUNTERO_SONIDO],HL
                LD      HL,INTERR
                SET     2,[HL]
                RET
;PLAYER OFF

PLAYER_OFF:	XOR	A			;***** IMPORTANTE SI NO HAY MUSICA ****
		LD	[INTERR],A

		LD	HL,PSG_REG
		LD	DE,PSG_REG+1
		LD	BC,14
		LD	[HL],A
		LDIR

		LD	HL,PSG_REG_SEC
		LD	DE,PSG_REG_SEC+1
		LD	BC,14
		LD	[HL],A
		LDIR

	
		LD      A,10111000B		; **** POR SI ACASO ****
		LD      [PSG_REG+7],A
		CALL	ROUT
		RET


;CARGA UNA CANCION
;IN:[A]=N� DE CANCION

CARGA_CANCION:  LD      HL,INTERR       ;CARGA CANCION
		
                SET     1,[HL]          ;REPRODUCE CANCION
                LD      HL,SONG
                LD      [HL],A          ;N� A

                

;DECODIFICAR
;IN-> INTERR 0 ON
;     SONG

;CARGA CANCION SI/NO

DECODE_SONG:    LD      A,[SONG]

;LEE CABECERA DE LA CANCION
;BYTE 0=TEMPO

                LD      HL,TABLA_SONG
                CALL    EXT_WORD
                LD      A,[HL]
                LD      [TEMPO],A
		XOR	A
		LD	[TTEMPO],A
                
;HEADER BYTE 1
;[-|-|-|-|-|-|-|LOOP]

                INC	HL		;LOOP 1=ON/0=OFF?
                LD	A,[HL]
                BIT	0,A
                JR	Z,NPTJP0
                PUSH	HL
                LD	HL,INTERR
                SET	4,[HL]
                POP	HL
             
                
NPTJP0:         INC	HL		;2 BYTES RESERVADOS
                INC	HL
                INC	HL

;BUSCA Y GUARDA INICIO DE LOS CANALES EN EL MODULO MUS

		
		LD	[PUNTERO_P_DECA],HL
		LD	E,$3F			;CODIGO INTRUMENTO 0
		LD	B,$FF			;EL MODULO DEBE TENER UNA LONGITUD MENOR DE $FF00 ... o_O!
BGICMODBC1:	XOR	A			;BUSCA EL BYTE 0
		CPIR
		DEC	HL
		DEC	HL
		LD	A,E			;ES EL INSTRUMENTO 0??
		CP	[HL]
		INC	HL
		INC	HL
		JR	Z,BGICMODBC1

		LD	[PUNTERO_P_DECB],HL

BGICMODBC2:	XOR	A			;BUSCA EL BYTE 0
		CPIR
		DEC	HL
		DEC	HL
		LD	A,E
		CP	[HL]			;ES EL INSTRUMENTO 0??
		INC	HL
		INC	HL
		JR	Z,BGICMODBC2

		LD	[PUNTERO_P_DECC],HL
		
BGICMODBC3:	XOR	A			;BUSCA EL BYTE 0
		CPIR
		DEC	HL
		DEC	HL
		LD	A,E
		CP	[HL]			;ES EL INSTRUMENTO 0??
		INC	HL
		INC	HL
		JR	Z,BGICMODBC3
		LD	[PUNTERO_P_DECP],HL
		
                
;LEE DATOS DE LAS NOTAS
;[|][|||||] LONGITUD\NOTA

INIT_DECODER:   LD      DE,[CANAL_A]
                LD      [PUNTERO_A],DE
                LD	HL,[PUNTERO_P_DECA]
                CALL    DECODE_CANAL    ;CANAL A
                LD	[PUNTERO_DECA],HL
                
                LD      DE,[CANAL_B]
                LD      [PUNTERO_B],DE
                LD	HL,[PUNTERO_P_DECB]
                CALL    DECODE_CANAL    ;CANAL B
                LD	[PUNTERO_DECB],HL
                
                LD      DE,[CANAL_C]
                LD      [PUNTERO_C],DE
                LD	HL,[PUNTERO_P_DECC]
                CALL    DECODE_CANAL    ;CANAL C
                LD	[PUNTERO_DECC],HL
                
                LD      DE,[CANAL_P]
                LD      [PUNTERO_P],DE
                LD	HL,[PUNTERO_P_DECP]
                CALL    DECODE_CANAL    ;CANAL P
                LD	[PUNTERO_DECP],HL
               
                RET


;DECODIFICA NOTAS DE UN CANAL
;IN [DE]=DIRECCION DESTINO
;NOTA=0 FIN CANAL
;NOTA=1 SILENCIO
;NOTA=2 PUNTILLO
;NOTA=3 COMANDO I

DECODE_CANAL:   LD      A,[HL]
                AND     A               ;FIN DEL CANAL?
                JR      Z,FIN_DEC_CANAL
                CALL    GETLEN

                CP      00000001B       ;ES SILENCIO?
                JR      NZ,NO_SILENCIO
                SET     6,A
                JR      NO_MODIFICA
                
NO_SILENCIO:    CP      00111110B       ;ES PUNTILLO?
                JR      NZ,NO_PUNTILLO
                OR      A
                RRC     B
                XOR     A
                JR      NO_MODIFICA

NO_PUNTILLO:    CP      00111111B       ;ES COMANDO?
                JR      NZ,NO_MODIFICA
                BIT     0,B             ;COMADO=INSTRUMENTO?
                JR      Z,NO_INSTRUMENTO   
                LD      A,11000001B     ;CODIGO DE INSTRUMENTO      
                LD      [DE],A
                INC     HL
                INC     DE
                LD      A,[HL]          ;N� DE INSTRUMENTO
                LD      [DE],A
                INC     DE
                INC	HL
                JR      DECODE_CANAL
                
NO_INSTRUMENTO: BIT     2,B
                JR      Z,NO_ENVOLVENTE
                LD      A,11000100B     ;CODIGO ENVOLVENTE
                LD      [DE],A
                INC     DE
                INC	HL
                JR      DECODE_CANAL
     
NO_ENVOLVENTE:  BIT     1,B
                JR      Z,NO_MODIFICA           
                LD      A,11000010B     ;CODIGO EFECTO
                LD      [DE],A                  
                INC     HL                      
                INC     DE                      
                LD      A,[HL]                  
                CALL    GETLEN   
                
NO_MODIFICA:    LD      [DE],A
                INC     DE
                XOR     A
                DJNZ    NO_MODIFICA
		SET     7,A
		SET 	0,A
                LD      [DE],A
                INC     DE
                INC	HL
                RET			;** JR      DECODE_CANAL
                
FIN_DEC_CANAL:  SET     7,A
                LD      [DE],A
                INC     DE
                RET

GETLEN:         LD      B,A
                AND     00111111B
                PUSH    AF
                LD      A,B
                AND     11000000B
                RLCA
                RLCA
                INC     A
                LD      B,A
                LD      A,10000000B
DCBC0:          RLCA
                DJNZ    DCBC0
                LD      B,A
                POP     AF
                RET
                
                

        
                
;PLAY __________________________________________________


PLAY:          	LD      HL,INTERR       ;PLAY BIT 1 ON?
                BIT     1,[HL]
                RET     Z
;TEMPO          
                LD      HL,TTEMPO       ;CONTADOR TEMPO
                INC     [HL]
                LD      A,[TEMPO]
                CP      [HL]
                JR      NZ,PAUTAS
                LD      [HL],0
                
;INTERPRETA      
                LD      IY,PSG_REG
                LD      IX,PUNTERO_A
                LD      BC,PSG_REG+8
                CALL    LOCALIZA_NOTA
                LD      IY,PSG_REG+2
                LD      IX,PUNTERO_B
                LD      BC,PSG_REG+9
                CALL    LOCALIZA_NOTA
                LD      IY,PSG_REG+4
                LD      IX,PUNTERO_C
                LD      BC,PSG_REG+10
                CALL    LOCALIZA_NOTA
                LD      IX,PUNTERO_P    ;EL CANAL DE EFECTOS ENMASCARA OTRO CANAL
                CALL    LOCALIZA_EFECTO              

;PAUTAS 
                
PAUTAS:         LD      IY,PSG_REG+0
                LD      IX,PUNTERO_P_A
                LD      HL,PSG_REG+8
                CALL    PAUTA           ;PAUTA CANAL A
                LD      IY,PSG_REG+2
                LD      IX,PUNTERO_P_B
                LD      HL,PSG_REG+9
                CALL    PAUTA           ;PAUTA CANAL B
                LD      IY,PSG_REG+4
                LD      IX,PUNTERO_P_C
                LD      HL,PSG_REG+10
                CALL    PAUTA           ;PAUTA CANAL C                

                RET
                


;REPRODUCE EFECTOS DE SONIDO 

REPRODUCE_SONIDO:

		LD      HL,INTERR   
                BIT     2,[HL]          ;ESTA ACTIVADO EL EFECTO?
                RET     Z
                LD      HL,[PUNTERO_SONIDO]
                LD      A,[HL]
                CP      $FF
                JR      Z,FIN_SONIDO
                LD      [PSG_REG_SEC+0],A
                INC     HL
                LD      A,[HL]
                RRCA
                RRCA
                RRCA
                RRCA
                AND     00001111B
                LD      [PSG_REG_SEC+1],A
                LD      A,[HL]
                AND     00001111B
                LD      [PSG_REG_SEC+8],A
                INC     HL
                LD      A,[HL]
                AND     A
                JR      Z,NO_RUIDO
                LD      [PSG_REG_SEC+6],A
                LD      A,10110000B
                JR      SI_RUIDO
NO_RUIDO:       LD      A,10111000B
SI_RUIDO:       LD      [PSG_REG_SEC+7],A
       
                INC     HL
                LD      [PUNTERO_SONIDO],HL
                RET
FIN_SONIDO:     LD      HL,INTERR
                RES     2,[HL]

FIN_NOPLAYER:	LD      A,10111000B
       		LD      [PSG_REG+7],A
                RET         
                
;VUELCA BUFFER DE SONIDO AL PSG

ROUT:      	XOR     A
ROUT_A0:        LD      DE,$FFBF
                LD      BC,$FFFD
                LD      HL,PSG_REG_SEC
LOUT:           OUT     [C],A
                LD      B,E
                OUTI 
                LD      B,D
                INC     A
                CP      13
                JR      NZ,LOUT
                OUT     [C],A
                LD      A,[HL]
                AND     A
                RET     Z
                LD      B,E
                OUTI
                XOR     A
                LD      [PSG_REG_SEC+13],A
                LD	[PSG_REG+13],A
                RET


;LOCALIZA NOTA CANAL A
;IN [PUNTERO_A]

LOCALIZA_NOTA:  LD      L,[IX+0]       		;HL=[PUNTERO_A_C_B]
                LD      H,[IX+1]
                LD      A,[HL]
                AND     11000000B      		;COMANDO?
                CP      11000000B
                JR      NZ,LNJP0

;BIT[0]=INSTRUMENTO
                
COMANDOS:       LD      A,[HL]
                BIT     0,A             	;INSTRUMENTO
                JR      Z,COM_EFECTO

                INC     HL
                LD      A,[HL]          	;N� DE PAUTA
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                LD      HL,TABLA_PAUTAS
                CALL    EXT_WORD
                LD      [IX+18],L
                LD      [IX+19],H
                LD      [IX+12],L
                LD      [IX+13],H
                LD      L,C
                LD      H,B
                RES     4,[HL]        		;APAGA EFECTO ENVOLVENTE
                XOR     A
                LD      [PSG_REG_SEC+13],A
                LD	[PSG_REG+13],A
                JR      LOCALIZA_NOTA

COM_EFECTO:     BIT     1,A             	;EFECTO DE SONIDO
                JR      Z,COM_ENVOLVENTE

                INC     HL
                LD      A,[HL]
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                CALL    INICIA_SONIDO
                RET

COM_ENVOLVENTE: BIT     2,A
                RET     Z               	;IGNORA - ERROR            
           
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                LD      L,C
                LD      H,B
                LD	[HL],00010000B          ;ENCIENDE EFECTO ENVOLVENTE
                JR      LOCALIZA_NOTA
                
              
LNJP0:          LD      A,[HL]
                INC     HL
                BIT     7,A
                JR      Z,NO_FIN_CANAL_A	;
                BIT	0,A
                JR	Z,FIN_CANAL_A

FIN_NOTA_A:	LD      E,[IX+6]
		LD	D,[IX+7]	;PUNTERO BUFFER AL INICIO
		LD	[IX+0],E
		LD	[IX+1],D
		LD	L,[IX+30]	;CARGA PUNTERO DECODER
		LD	H,[IX+31]
		PUSH	BC
                CALL    DECODE_CANAL    ;DECODIFICA CANAL
                POP	BC
                LD	[IX+30],L	;GUARDA PUNTERO DECODER
                LD	[IX+31],H
                JP      LOCALIZA_NOTA
                
FIN_CANAL_A:    LD	HL,INTERR	;LOOP?
                BIT	4,[HL]              
                JR      NZ,FCA_CONT
                CALL	PLAYER_OFF
                RET

FCA_CONT:	LD	L,[IX+24]	;CARGA PUNTERO INICIAL DECODER
		LD	H,[IX+25]
		LD	[IX+30],L
		LD	[IX+31],H
		JR      FIN_NOTA_A
                
NO_FIN_CANAL_A: LD      [IX+0],L        ;[PUNTERO_A_B_C]=HL GUARDA PUNTERO
                LD      [IX+1],H
                AND     A               ;NO REPRODUCE NOTA SI NOTA=0
                JR      Z,FIN_RUTINA
                BIT     6,A             ;SILENCIO?
                JR      Z,NO_SILENCIO_A
                LD	A,[BC]
                AND	00010000B
                JR	NZ,SILENCIO_ENVOLVENTE
                XOR     A
                LD	[BC],A		;RESET VOLUMEN
                LD	[IY+0],A
                LD	[IY+1],A
		RET
		
SILENCIO_ENVOLVENTE:
		LD	A,$FF
                LD	[PSG_REG+11],A
                LD	[PSG_REG+12],A               
                XOR	A
                LD	[PSG_REG+13],A                               
                LD	[IY+0],A
                LD	[IY+1],A
                RET

NO_SILENCIO_A:  CALL    NOTA            ;REPRODUCE NOTA
                LD      L,[IX+18]       ;HL=[PUNTERO_P_A0] RESETEA PAUTA 
                LD      H,[IX+19]
                LD      [IX+12],L       ;[PUNTERO_P_A]=HL
                LD      [IX+13],H
FIN_RUTINA:     RET


;LOCALIZA EFECTO
;IN HL=[PUNTERO_P]

LOCALIZA_EFECTO:LD      L,[IX+0]       ;HL=[PUNTERO_P]
                LD      H,[IX+1]
                LD      A,[HL]
                CP      11000010B
                JR      NZ,LEJP0

                INC     HL
                LD      A,[HL]
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                CALL    INICIA_SONIDO
                RET
            
              
LEJP0:          INC     HL
                BIT     7,A
                JR      Z,NO_FIN_CANAL_P	;
                BIT	0,A
                JR	Z,FIN_CANAL_P
FIN_NOTA_P:	LD      DE,[CANAL_P]
		LD	[IX+0],E
		LD	[IX+1],D
		LD	HL,[PUNTERO_DECP]	;CARGA PUNTERO DECODER
		PUSH	BC
		CALL    DECODE_CANAL    	;DECODIFICA CANAL
		POP	BC
                LD	[PUNTERO_DECP],HL	;GUARDA PUNTERO DECODER
                JP      LOCALIZA_EFECTO
                
FIN_CANAL_P:	LD	HL,[PUNTERO_P_DECP]	;CARGA PUNTERO INICIAL DECODER
		LD	[PUNTERO_DECP],HL
		JR      FIN_NOTA_P
                
NO_FIN_CANAL_P: LD      [IX+0],L        ;[PUNTERO_A_B_C]=HL GUARDA PUNTERO
                LD      [IX+1],H
                RET
                
; PAUTA DE LOS 3 CANALES
; IN:[IX]:PUNTERO DE LA PAUTA
;    [HL]:REGISTRO DE VOLUMEN
;    [IY]:REGISTROS DE FRECUENCIA

; FORMATO PAUTA	
;	    7    6     5     4   3-0                     3-0  
; BYTE 1 [LOOP|OCT-1|OCT+1|SLIDE|VOL] - BYTE 2 [ | | | |PITCH]

PAUTA:          BIT     4,[HL]        ;SI LA ENVOLVENTE ESTA ACTIVADA NO ACTUA PAUTA
                RET     NZ

		LD	A,[IY+0]
		LD	B,[IY+1]
		OR	B
		RET	Z


                PUSH	HL
                ;LD      L,[IX+0]
                ;LD      H,[IX+1]
                		
                ;LD	A,[HL]		;COMPRUEBA SLIDE BIT 4
		;BIT	4,A
		;JR	Z,PCAJP4
		;LD	L,[IY+0]	;FRECUENCIA FINAL
		;LD	H,[IY+1]
		;SBC	HL,DE
		;JR	Z,PCAJP4
		;JR	C,SLIDE_POS
		;EX	DE,HL
		;RRC	D		;/4
		;RR	E
		;RRC	D
		;RR	E


		;ADC	HL,DE
		;LD	[IY+0],L
		;LD	[IY+1],H
SLIDE_POS:		
		;POP	HL
		;RET
                
PCAJP4:         LD      L,[IX+0]
                LD      H,[IX+1]         
		LD	A,[HL]
		
		BIT     7,A		;LOOP / EL RESTO DE BITS NO AFECTAN
                JR      Z,PCAJP0
                AND     00011111B       ;LOOP PAUTA [0,32]X2!!!-> PARA ORNAMENTOS
                RLCA			;X2
                LD      D,0
                LD      E,A
                SBC     HL,DE
                LD      A,[HL]

PCAJP0:		BIT	6,A		;OCTAVA -1
		JR	Z,PCAJP1
		LD	E,[IY+0]
		LD	D,[IY+1]

		AND	A
		RRC	D
		RR	E
		LD	[IY+0],E
		LD	[IY+1],D
		JR	PCAJP2
		
PCAJP1:		BIT	5,A		;OCTAVA +1
		JR	Z,PCAJP2
		LD	E,[IY+0]
		LD	D,[IY+1]

		AND	A
		RLC	E
		RL	D
		LD	[IY+0],E
		LD	[IY+1],D		


PCAJP2:		INC     HL
		PUSH	HL
		LD	E,A
		LD	A,[HL]		;PITCH DE FRECUENCIA
		LD	L,A
		AND	A
		LD	A,E
		JR	Z,ORNMJP1

                LD	A,[IY+0]	;SI LA FRECUENCIA ES 0 NO HAY PITCH
                ADD	A,[IY+1]
                AND	A
                LD	A,E
                JR	Z,ORNMJP1
                

		BIT	7,L
		JR	Z,ORNNEG
		LD	H,$FF
		JR	PCAJP3
ORNNEG:		LD	H,0
		
PCAJP3:		LD	E,[IY+0]
		LD	D,[IY+1]
		ADC	HL,DE
		LD	[IY+0],L
		LD	[IY+1],H
ORNMJP1:	POP	HL
		
		INC	HL
                LD      [IX+0],L
                LD      [IX+1],H
PCAJP5:         POP	HL
                AND	00001111B	;VOLUMEN FINAL
                LD      [HL],A
                RET



;NOTA : REPRODUCE UNA NOTA
;IN [A]=CODIGO DE LA NOTA
;   [IY]=REGISTROS DE FRECUENCIA


NOTA:           ;ADD	6		;*************************
		LD      L,C
                LD      H,B
                BIT     4,[HL]
                LD      B,A
                JR	NZ,EVOLVENTES
                LD	A,B
                LD      HL,DATOS_NOTAS
                RLCA                    ;X2
                LD      D,0
                LD      E,A
                ADD     HL,DE
                LD      A,[HL]
                LD      [IY+0],A
                INC     HL
                LD      A,[HL]
                LD      [IY+1],A
                RET

;IN [A]=CODIGO DE LA ENVOLVENTE
;   [IY]=REGISTRO DE FRECUENCIA

EVOLVENTES:     
		PUSH	AF
		CALL	ENV_RUT1
		LD	DE,$0000
		LD      [IY+0],E
                LD      [IY+1],D		
	
		POP	AF	
		ADD	A,48
		CALL	ENV_RUT1
		
	
		LD	A,E
                LD      [PSG_REG+11],A
                LD	A,D
                LD      [PSG_REG+12],A
                LD      A,$0e
                LD      [PSG_REG+13],A
                RET

;IN[A] NOTA
ENV_RUT1:	LD      HL,DATOS_NOTAS
		RLCA                    ;X2
                LD      D,0
                LD      E,A
                ADD     HL,DE
                LD	E,[HL]
		INC	HL
		LD	D,[HL]
                RET



EXT_WORD:       LD      D,0
                SLA     A               ;*2
                LD      E,A
                ADD     HL,DE
                LD      E,[HL]
                INC     HL
                LD      D,[HL]
                EX      DE,HL
                RET

; ---------------------------------------------------------------------------------------------------------------------------

;FUNCIONES DEL PLAYER DE FX
;___________________________

;INICIA FX
;REPRODUCE_EFECTO
;FIN_EFECTO


;VARIBLES Y FUNCIONES EXTERNAS
;_____________________________

;TABLA_EFECTOS		DW TABLA DE DIRECCIONES DE LOS EFECTOS
;INTERR			DB 
;EXT_WORD		FUNCION DE EXTRACCION DE LA DIRECCION DEL FX POR N� DE ORDEN


; *** AJUSTAR CANAL DE EFECTOS ANTES DE INICIAR EL REPRODUCTOR 
; *** LLAMAR EN CADA INTERRUPCION A REPRODUCE_EFECTO

; VARIABLES
;___________

;INTERR:         DB     00              ;INTERRUPTORES 1=ON 0=OFF       
                                        ;BIT 0=CARGA CANCION ON/OFF
                                        ;BIT 1=PLAYER ON/OFF
                                        ;BIT 2=SONIDOS ON/OFF
                                        
					;BIT 3=EFECTOS ON/OFF 
;EFECTOS

N_EFECTO:
         DB 0 ; EQU	$E450   ;DB : NUMERO DE SONIDO
PUNTERO_EFECTO:
         DW 0 ; EQU	$E451	;DW : PUNTERO DEL SONIDO QUE SE REPRODUCE;REPRODUCE EFECTOS
CANAL_EFECTOS:
         DB 0 ; EQU	$E453	;DB : 1:CANAL A - 2:CANAL B - OTRO:CANAL C


INICIA_EFECTO:	LD	A,B
		LD      HL,TABLA_EFECTOS
                CALL    EXT_WORD
                LD      [PUNTERO_EFECTO],HL
		LD      HL,INTERR
                SET     3,[HL]
                RET       

REPRODUCE_EFECTO:

                LD      HL,INTERR   
                BIT     3,[HL]          	;ESTA ACTIVADO EL EFECTO?
                RET     Z
                LD      HL,[PUNTERO_EFECTO]
                LD      A,[HL]
                CP      $FF
                JP      Z,FIN_EFECTO
                LD	B,A			;FRECUENCIA FINO
                INC     HL
                LD	A,[HL]
                RRCA
                RRCA
                RRCA
                RRCA
                AND     00001111B
                LD	C,A			;	FRECUENCIA GRAVE
		;LD      A,10111000B		;	ELIMINA RUIDO
       		;LD      [PSG_REG_SEC+7],A
                LD      A,[HL]
                DEC	A			;DEC A PARA BAJR VOLUMEN!! O PONER VARIABLE
                ;DEC	A
                AND     00001111B

                LD	D,A			;VOLUMEN
                INC     HL			;INCREMENTA Y GUARDA EL PUNTERO
                LD      [PUNTERO_EFECTO],HL     
           	LD	IX,PSG_REG_SEC
                LD	A,3			;SELECCION DE CANAL *********
                CP	1
                JR	Z,RS_CANALA
                CP	2
		JR	Z,RS_CANALB
		
RS_CANALC:  	LD      [IX+4],B
		LD      [IX+5],C
                LD      [IX+10],D
                RET		
		
RS_CANALA:	LD      [IX+0],B
		LD      [IX+1],C
                LD      [IX+8],D
                RET
                
RS_CANALB:	LD      [IX+2],B
		LD      [IX+3],C
                LD      [IX+9],D
                RET
                
FIN_EFECTO:     LD      HL,INTERR
                RES     3,[HL]			;DESACTIVA EFECTO
                RET         

; ---------------------------------------------------------------------------------------------------------------------------



;BANCO DE INSTRUMENTOS 2 BYTES POR INT.

;[0][RET 2 OFFSET]
;[1][+-PITCH]


;BANCO DE INSTRUMENTOS 2 BYTES POR INT.

;[0][RET 2 OFFSET]
;[1][+-PITCH]

TABLA_PAUTAS: DW 	PAUTA_2,PAUTA_3,PAUTA_4,PAUTA_5,PAUTA_6,PAUTA_7,PAUTA_8,PAUTA_9,PAUTA_10;,PAUTA_11,PAUTA_12,PAUTA_13,PAUTA_14,PAUTA_15,PAUTA_16,PAUTA_17,PAUTA_18


		.INCLUDE	"cray5_a.mus.asm"





;DATOS DE LOS EFECTOS DE SONIDO

;EFECTOS DE SONIDO



TABLA_SONIDOS:  DW      SONIDO1,SONIDO2,SONIDO3,SONIDO4;,SONIDO5;,SONIDO6,SONIDO7;,SONIDO8


                

;DATOS MUSICA



TABLA_SONG:     DW      SONG_0,SONG_1,SONG_2,SONG_3          ;******** TABLA DE DIRECCIONES DE ARCHIVOS MUS

;DATOS_NOTAS:    .INCBIN "C:/EM/BRMSX/PLAYER/NOTAS.DAT"        ;DATOS DE LAS NOTAS


DATOS_NOTAS:    DW $0000,$0000
;		;DW $077C,$0708
;		DW $06B0,$0640,$05EC,$0594,$0544,$04F8,$04B0,$0470,$042C,$03FD
;		DW $03BE,$0384,$0358,$0320,$02F6,$02CA,$02A2,$027C,$0258,$0238,$0216,$01F8
;		DW $01DF,$01C2,$01AC,$0190,$017B,$0165,$0151,$013E,$012C,$011C,$010A,$00FC
;		DW $00EF,$00E1,$00D6,$00C8,$00BD,$00B2,$00A8,$009F,$0096,$008E,$0085,$007E
;		DW $0077,$0070,$006B,$0064,$005E,$0059,$0054,$004F,$004B,$0047,$0042,$003F
;		DW $003B,$0038,$0035,$0032,$002F,$002C,$002A,$0027,$0025,$0023,$0021,$001F
;		DW $001D,$001C,$001A,$0019,$0017,$0016,$0015,$0013,$0012,$0011,$0010,$000F
		

TABLA3:		;dw $0CDA,$0C22,$0B73,$0ACF,$0A33,$09A1,$0917,$0894,$0819,$07A4,$0737,$06CF,
		dw $066D,$0611,$05BA,$0567,$051A,$04D0,$048B,$044A,$040C,$03D2,$039B,$0367
		dw $0337,$0308,$02DD,$02B4,$028D,$0268,$0246,$0225,$0206,$01E9,$01CE,$01B4
		dw $019B,$0184,$016E,$015A,$0146,$0134,$0123,$0112,$0103,$00F5,$00E7,$00DA
		dw $00CE,$00C2,$00B7,$00AD,$00A3,$009A,$0091,$0089,$0082,$007A,$0073,$006D
		dw $0067,$0061,$005C,$0056,$0052,$004D,$0049,$0045,$0041,$003D,$003A,$0036
		dw $0033,$0031,$002E,$002B,$0029,$0027,$0024,$0022,$0020,$001F,$001D,$001B
		dw $001A,$0018,$0017,$0016,$0014,$0013,$0012,$0011,$0010,$000F,$000E,$000D


		;DW	$688,$62A,$5D2,$57E,$52F,$4E5,$49E,$45C,$41D,$3E2,$3AA,$376
		;DW	$344,$315,$2E9,$2BF,$297,$272,$24F,$22E,$20E,$1F1,$1D5,$1BB
		;DW	$1A2,$18A,$174,$15F,$14B,$139,$127,$117,$107,$F8,$EA,$DD
		;DW	$D1,$C5,$BA,$AF,$A5,$9C,$93,$8B,$83,$7C,$75,$6E
		;DW	$68,$62,$5D,$57,$52,$4E,$49,$45,$41,$3E,$3A,$37
		;DW	$34,$31,$2E,$2B,$29,$27,$24,$22,$20,$1F,$1D,$1B
		;DW	$1A,$18,$17,$15,$14,$13,$12,$11,$10,$F,$E,$D



SONG_0:		.INCBIN	"..\mus\cray5.mus"
SONG_1:         .INCBIN	"..\mus\cray5_ini.mus"
SONG_2:         .INCBIN	"..\mus\cray5_war.mus"
SONG_3:         .INCBIN	"..\mus\cray5_tension.mus"



; VARIABLES__________________________


INTERR:         DB     00               ;INTERRUPTORES 1=ON 0=OFF
                                        ;BIT 0=CARGA CANCION ON/OFF
                                        ;BIT 1=PLAYER ON/OFF
                                        ;BIT 2=SONIDOS ON/OFF
                                        ;BIT 3=EFECTOS ON/OFF

;MUSICA **** EL ORDEN DE LAS VARIABLES ES FIJO ******



SONG:           DB     00               ;DBN� DE CANCION
TEMPO:          DB     00               ;DB TEMPO
TTEMPO:         DB     00               ;DB CONTADOR TEMPO
PUNTERO_A:      DW     00               ;DW PUNTERO DEL CANAL A
PUNTERO_B:      DW     00               ;DW PUNTERO DEL CANAL B
PUNTERO_C:      DW     00               ;DW PUNTERO DEL CANAL C

CANAL_A:        DW     BUFFER_DEC       ;DW DIRECION DE INICIO DE LA MUSICA A
CANAL_B:        DW     00               ;DW DIRECION DE INICIO DE LA MUSICA B
CANAL_C:        DW     00               ;DW DIRECION DE INICIO DE LA MUSICA C

PUNTERO_P_A:    DW     00               ;DW PUNTERO PAUTA CANAL A
PUNTERO_P_B:    DW     00               ;DW PUNTERO PAUTA CANAL B
PUNTERO_P_C:    DW     00               ;DW PUNTERO PAUTA CANAL C

PUNTERO_P_A0:   DW     00               ;DW INI PUNTERO PAUTA CANAL A
PUNTERO_P_B0:   DW     00               ;DW INI PUNTERO PAUTA CANAL B
PUNTERO_P_C0:   DW     00               ;DW INI PUNTERO PAUTA CANAL C


PUNTERO_P_DECA:	DW     00		;DW PUNTERO DE INICIO DEL DECODER CANAL A
PUNTERO_P_DECB:	DW     00		;DW PUNTERO DE INICIO DEL DECODER CANAL B
PUNTERO_P_DECC:	DW     00		;DW PUNTERO DE INICIO DEL DECODER CANAL C

PUNTERO_DECA:	DW     00		;DW PUNTERO DECODER CANAL A
PUNTERO_DECB:	DW     00		;DW PUNTERO DECODER CANAL B
PUNTERO_DECC:	DW     00		;DW PUNTERO DECODER CANAL C       


;CANAL DE EFECTOS - ENMASCARA OTRO CANAL

PUNTERO_P:      DW     00           	;DW PUNTERO DEL CANAL EFECTOS
CANAL_P:        DW     00           	;DW DIRECION DE INICIO DE LOS EFECTOS
PUNTERO_P_DECP:	DW     00		;DW PUNTERO DE INICIO DEL DECODER CANAL P
PUNTERO_DECP:	DW     00		;DW PUNTERO DECODER CANAL P

PSG_REG:        DB      00,00,00,00,00,00,00,10111000B,00,00,00,00,00,00,00    ;DB [11] BUFFER DE REGISTROS DEL PSG
PSG_REG_SEC:    DB      00,00,00,00,00,00,00,10111000B,00,00,00,00,00,00,00    ;DB [11] BUFFER SECUNDARIO DE REGISTROS DEL PSG



;ENVOLVENTE_A    EQU     $D033           ;DB
;ENVOLVENTE_B    EQU     $D034           ;DB
;ENVOLVENTE_C    EQU     $D035           ;DB


;EFECTOS DE SONIDO

N_SONIDO:       DB      0               ;DB : NUMERO DE SONIDO
PUNTERO_SONIDO: DW      0               ;DW : PUNTERO DEL SONIDO QUE SE REPRODUCE

;EFECTOS

;N_EFECTO:       DB      0               ;DB : NUMERO DE SONIDO
;PUNTERO_EFECTO: DW      0               ;DW : PUNTERO DEL SONIDO QUE SE REPRODUCE

TABLA_EFECTOS:	DW	DISPARO_CRAY5, EXPLOSION_ENEMIGO_CRAY5, DANO_CRAY5, LLAVE_RECOGIDA_CRAY5, ABRIR_PUERTA_CRAY5, PULSAR_INTERRUPTOR_CRAY5, ATRACCION_IMAN_CRAY5, TELETRANSPORTE_CRAY5

DISPARO_CRAY5:

DB $2F,$0B
 DB $1B,$0B
 DB $35,$0C
 DB $1A,$0B
 DB $44,$0C
 DB $60,$0B
 DB $55,$0B
 DB $A1,$0A
 DB $C7,$09
 DB $54,$17

  DB $44,$08
  DB $60,$08
  DB $55,$07
  DB $A1,$06
  DB $C7,$05
 DB $54,$14

  DB $44,$05
  DB $60,$05
  DB $55,$04
  DB $A1,$04
  DB $C7,$03
 DB $54,$13

 DB $FF


EXPLOSION_ENEMIGO_CRAY5:

 
 DB $33,$1E
 DB $74,$1E
 DB $00,$2D
 DB $00,$3D
 DB $05,$4D
 DB $90,$0B
 DB $BB,$0B
 DB $23,$1C
 DB $64,$1C
 DB $00,$2B
 DB $00,$3B
 DB $05,$48
 DB $50,$58
 DB $B0,$07
 DB $BB,$07
 DB $23,$18
 DB $64,$18
 
  DB $23,$19
  DB $64,$19
  DB $00,$28
  DB $00,$37
  DB $05,$47
  DB $50,$56
  DB $B0,$05
  DB $BB,$04
  DB $23,$13
 DB $64,$18
  
  DB $23,$16
  DB $64,$16
  DB $00,$25
  DB $00,$35
  DB $05,$44
  DB $50,$54
  DB $B0,$03
  DB $BB,$03
  DB $23,$13
 DB $64,$12
 
  DB $FF
  
  
 DANO_CRAY5:
 

  DB $0F,$16
  DB $09,$19
  DB $18,$2A
  DB $08,$38
  
    DB $2C,$16
    DB $0F,$16
    DB $09,$15
    DB $18,$24
  DB $08,$33
  
    DB $2C,$14
    DB $0F,$14
    DB $09,$13
    DB $18,$22
  DB $08,$31
  
  
     DB $2C,$15
      DB $0F,$15
      DB $09,$14
      DB $18,$23
    DB $08,$32
    
      DB $2C,$13
      DB $0F,$13
      DB $09,$12
      DB $18,$22
    DB $08,$31
 
 DB $FF
 
 
 
LLAVE_RECOGIDA_CRAY5:
 
  DB $40,$0B
  DB $C0,$0D
  DB $40,$0E
  DB $40,$0F
  DB $30,$0D
  DB $20,$0C
  DB $30,$0B
  DB $20,$0A
  DB $10,$09
  
    DB $40,$08
    DB $C0,$09
    DB $40,$08
    DB $40,$08
    DB $30,$07
    DB $20,$06
    DB $30,$05
    DB $20,$04
    DB $10,$03

  DB $20,$0A
  DB $A0,$0B
  DB $20,$0B
  DB $20,$0A
  DB $10,$08
  DB $20,$06
  DB $10,$05
  DB $A0,$04
  DB $20,$06
  DB $A0,$05

   DB $20,$08
  DB $A0,$09
  DB $20,$09
  DB $20,$08
  DB $10,$06
  DB $20,$06
  DB $10,$05
  DB $A0,$04
  DB $20,$03
  DB $A0,$03
  DB $20,$02
  DB $20,$02
  DB $10,$02
  DB $20,$02
  DB $10,$02 
   
  
    DB $20,$04
    DB $A0,$04
    DB $20,$03
    DB $20,$03
    DB $10,$03
    DB $20,$03
    DB $10,$02

  
 DB $FF
 
 ABRIR_PUERTA_CRAY5:
 
 
 	DB $00,$38
 	DB $90,$2C
 	DB $90,$1A
 	DB $90,$2C
 	DB $90,$2A
 	DB $90,$2C
 	DB $90,$2A
 	DB $90,$2C
 	DB $90,$2A
 	DB $90,$2C
 	DB $90,$2A


 	DB $90,$29
 	DB $90,$2B
 	DB $90,$29
 	DB $90,$2B
 	DB $90,$29
 	DB $90,$2B
 	DB $90,$29

 
  	DB $90,$28
  	DB $90,$2B
  	DB $90,$28
  	DB $90,$2B
  	DB $90,$27
  	DB $90,$2A
 	DB $90,$26

 
 
  	DB $90,$27
  	DB $90,$29
  	DB $90,$27
  	DB $90,$29
  	DB $90,$27
  	DB $90,$28
  	DB $90,$25
 
  
   	DB $90,$25
   	DB $90,$27
   	DB $90,$24
   	DB $90,$27
   	DB $90,$23
   	DB $90,$26
 	DB $90,$22
 	
 	
 	
 
 		DB $00,$4A
 	 	DB $90,$5B
	 	DB $90,$6A
	 	DB $90,$88
	 	
	 	
 		DB $00,$49
 	 	DB $90,$5A
	 	DB $90,$69
	 	DB $90,$88
	 	
 		DB $00,$48
 	 	DB $90,$59
	 	DB $90,$68
	 	DB $90,$87
	 	
	 	

	 	
 	
 		DB $00,$5A
 	 	DB $90,$59
	 	DB $90,$68
	 	DB $90,$87	

 		DB $00,$59
 	 	DB $90,$58
	 	DB $90,$67
	 	DB $90,$86
	 	
 		DB $00,$58
 	 	DB $90,$57
	 	DB $90,$66
	 	DB $90,$85	 		 	

	 	
 		DB $00,$58
 	 	DB $90,$57
	 	DB $90,$66
	 	DB $90,$85
	 	
	 	
 		DB $00,$57
 	 	DB $90,$56
	 	DB $90,$65
	 	DB $90,$84
	 	 	
 		DB $00,$56
 	 	DB $90,$55
	 	DB $90,$64
	 	DB $90,$83	
	 		 	 	
	 	 	
	 	 	
 DB $FF	
 
 
 PULSAR_INTERRUPTOR_CRAY5:
 
  DB $33,$08
  DB $13,$09
  DB $77,$0A
  DB $33,$0C
  DB $13,$0B
  DB $77,$0B
  DB $97,$0A
  DB $33,$0A
  DB $13,$09
  DB $77,$09
  DB $90,$08
  DB $30,$09
  DB $10,$08
  DB $70,$07
  DB $90,$06
  DB $30,$07
  DB $10,$06
  DB $70,$06
  DB $90,$05
  DB $30,$06
  DB $10,$05
  DB $70,$05
  DB $90,$04
 
 
   DB $90,$05
   DB $30,$05
   DB $10,$06
   DB $70,$06
   DB $90,$04
   DB $30,$04
   DB $10,$06
   DB $70,$06
   DB $90,$03
   DB $30,$03
   DB $10,$04

  
  
 DB $FF
 
 
 ATRACCION_IMAN_CRAY5:
 
 
 	DB	$00,$47
 	DB	$20,$47
 	DB	$48,$48
 	DB	$6A,$48
 	DB	$48,$49
 	DB	$24,$49
 	DB	$00,$4A
 	DB	$20,$4A
 	DB	$40,$4B
 	DB	$60,$4B
 	DB	$40,$4C
 	DB	$20,$4C
 	DB	$00,$4C
 	DB	$00,$4C
 	DB	$20,$4D
 	DB	$48,$4D
 	DB	$6A,$4D
 	DB	$48,$4D
 	DB	$24,$4D
 	DB	$00,$4D
 	DB	$20,$4D
 	DB	$40,$4D
 	DB	$60,$4D
 	DB	$40,$4D
 	DB	$20,$4C
 	DB	$00,$4C
 	
  	DB	$00,$4C
  	DB	$20,$4C
  	DB	$48,$4B
  	DB	$6A,$4B
  	DB	$48,$4B
  	DB	$24,$4B
  	DB	$00,$4A
  	DB	$20,$4A
  	DB	$40,$4A
  	DB	$60,$4A
  	DB	$40,$4A
  	DB	$20,$49
 	DB	$00,$49
 
  	DB	$00,$48
  	DB	$20,$48
  	DB	$48,$48
  	DB	$6A,$48
  	DB	$48,$47
  	DB	$24,$47
  	DB	$00,$47
  	DB	$20,$47
  	DB	$40,$46
  	DB	$60,$46
  	DB	$40,$45
  	DB	$20,$45
 	DB	$00,$44


DB	$FF 	


TELETRANSPORTE_CRAY5:


	DB	$10,$07
	DB	$20,$08
	DB	$40,$09

	DB	$0B,$08
	DB	$1B,$09
	DB	$30,$0A
	
	DB	$08,$09
	DB	$14,$0A
	DB	$20,$0B



	DB	$80,$0A
	DB	$00,$1B
	DB	$00,$2C
	DB	$10,$2D
	DB	$10,$2C
	
	DB	$20,$06
	DB	$40,$07
	DB	$80,$18
	DB	$00,$17
	DB	$10,$16
	DB	$10,$15

	DB	$40,$04
	DB	$80,$05
	DB	$00,$16
	DB	$00,$25
	DB	$10,$24
	DB	$10,$23	
	

	DB	$40,$03
	DB	$80,$04
	DB	$00,$15
	DB	$00,$24
	DB	$10,$23
	DB	$10,$22
	

DB	$FF



BUFFER_DEC:     DB      $00		;************************* mucha atencion!!!!
					; aqui se decodifica la cancion hay que dejar suficiente espacio libre.
					;*************************