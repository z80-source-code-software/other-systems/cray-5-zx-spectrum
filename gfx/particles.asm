#asm
; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      (256, 192)
;Char Size:       ( 32,  24)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Gfx
;Interleave:      Sprite
;Mask:            No

_particle1:
	DEFB	  0,  6, 14, 12, 16,  0,  0,  0
	DEFB	  4, 10,  1, 17,  3,  7,  6,  0
	DEFB	  0,  2,  0,  1,  0, 97,114, 60
	DEFB	  0, 96,224,192,128,128, 66, 40
	DEFB	 60, 78,134,  0,128,  0, 64,  0
	DEFB	 20, 34, 14, 30, 30, 12,  0,  0
	DEFB	  8,  2,  0, 24, 24,  0,  0,  0
	DEFB	  0,  0,  0, 20, 42,  0,  0,  0
	DEFB	  0,  0, 66,  0,  0, 36,  0,  0
	DEFB	  0,  0,  0,  0,129,  0,  0, 34
	DEFB	  0,  0,  0,  0,  0, 66,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,129,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 66

_particle2:
	DEFB	  0,  0,  0, 24, 24,  0,  0,  0
	DEFB	  0,  0, 24, 60, 60, 24,  0,  0
	DEFB	  0, 44, 66, 88, 26, 66, 52,  0
	DEFB	 74,129, 40,148, 41, 20,129, 82
	DEFB	129,  0,  8, 32,  4, 16,  0,129
	DEFB	  0,  0,  2, 64,  1, 32,  0,  0
	DEFB	  0,  0,  0,  0,130,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0, 68,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,130,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 65

_particle3:
    DEFB	  0,  0,  8, 20,  8,  0,  0,  0
	DEFB	  0, 20, 42, 20, 42, 20,  0,  0
	DEFB	 42, 85, 34, 73, 34, 85, 42,  0
	DEFB	 34, 65,  8, 20,  8, 65, 34,  0
	DEFB	 65,  8, 20, 34, 20,  8, 65,  0
	DEFB	  0, 34, 65,  0,  0, 65, 34,  0
	DEFB	  0, 65,  0,  0,  0,  0, 65,  0

#endasm
