/*
    Cray-5 A Sinclair ZX Spectrum Game
    Copyleft (C) 2009 Luis I. Garcia Ventura

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <spritepack.h>

#pragma output STACKPTR = 24599

#define AD_FREE 60000 // 60400
#define SENS 2
#define MAXPARTICLE 4
#define MAXENEMY 3
#define MAXFIRE 3
#define VELENEMY 4
#define DEEXO 24600
#define FRAME 24946
#define MAP 49152

// GFX - Page 4

#define GFXFRAME 49152
#define GFXTRANSP 49497
#define GFXMENU 50982
#define GFXGAMEOVER 51995
#define GFXFINISHED 54485
#define GFXBANNER 57385
#define ATTRSCR 57673

// WYZPlayer - Page 1

#define INICIAEFECTO 0xC459
#define WYZPLAYERISR 0xC030
#define CARGA_CANCION 0xC07C
#define SILENCIA_PLAYER 0xC057

#define FXFIRE            0
#define FXEXPLOSION       1
#define FXDAMAGE          2
#define FXKEY             3
#define FXDOOR            4
#define FXSWITCH          5
#define FXMAGNET          6
#define FXTELEPORT        7

// ASM Routines - Page 3

#define PRINTSTR_LINE 0xE663
#define MENU_ShowHOF 0xE736
#define MENU_ShowMenu 0xE7E5
#define CHECKHiScore 0xE860
#define NEW_HISCORE 0xE8B2
#define ER_Hook	0xE916


void *my_malloc(unsigned int bytes)
{
     return sp_BlockAlloc(0);
}

void *u_malloc = my_malloc;
void *u_free = sp_FreeBlock;

extern uchar *sp_NullSprPtr;
#asm
    LIB SPNullSprPtr
    ._sp_NullSprPtr  defw SPNullSprPtr
#endasm

extern struct sp_Rect *sp_ClipStruct;
#asm
    LIB SPCClipStruct
    ._sp_ClipStruct
    defw SPCClipStruct    
    CSCROLL_Stack:
    DRGVAR1:
        DEFW 0
    CURSOR_PAUSE:
    DRGVAR2:
        DEFW 0

#endasm

unsigned int asm_int;
#asm
DRGVAR3:
        DEFW 0
#endasm

extern unsigned char Tile0[];
extern unsigned char SuperTile_0[];
extern unsigned char SuperTile_71[];
extern unsigned char SuperTile_80[];
extern unsigned char SuperTile_105[];
extern unsigned char Charset[];
extern unsigned char Astronaut1[], Astronaut2[];
extern unsigned char /*Particle1[], Particle2[],*/ Particle3[];
extern unsigned char Arrows[];
extern unsigned char Enemies[];
extern unsigned char FireSpr [];
extern unsigned char Explosion [];


struct sp_Rect GameField = {3, 1, 21, 30};
struct sp_Rect TransFrame = {9, 10, 9, 9};

typedef struct {
  unsigned int x, y, LastX, LastY;
  signed int VelX, VelY;
  unsigned char *PrevFrame, *CurFrame;
  unsigned char Facing, Frame, KF;
  struct sp_SS *Sprite;
} ASTRONAUT;

ASTRONAUT Astronaut;

typedef struct {
  unsigned char Frame, KF;
  unsigned char *PrevFrame, *CurFrame;
  struct sp_SS *Sprite;
} STATIC_SPRITE;

STATIC_SPRITE DeadAstronaut;

typedef struct {
  unsigned int x, y;
  unsigned char Sort, Frame, KF;
  signed char VelX, VelY;
  unsigned char *PrevFrame, *CurFrame;
  struct sp_SS *Sprite;
} ENEMY;

ENEMY Enemy [MAXENEMY];

typedef struct {
  unsigned char x, y;
  unsigned char Sort, Frame, KF;
  unsigned char *PrevFrame, *CurFrame;
  struct sp_SS *Sprite;
} PARTICLE;

PARTICLE Particle[MAXPARTICLE];

typedef struct {
    signed char VelX;
    struct sp_SS *Sprite;
} FIRE;

FIRE Fire[MAXFIRE];

typedef struct {
  struct sp_SS *Sprite;
} SELECTOR;

SELECTOR Selector[2];

typedef struct {
  unsigned char *PrevFrame, *CurFrame;
  char Obj;
} SELECTORD;

SELECTORD SelectData;

unsigned char k, n, m, o, p, *pp, ST;
void *JoyFunc;
unsigned char Ticks, ColorTime;
unsigned char ActRoom;
unsigned char RoomBuffer [70];

//unsigned char PartLastFrame [3] = {14, 10, 8};
//unsigned char* PartInitFrame [3] = {Particle1, Particle2, Particle3};
unsigned int Score, Time;
unsigned char Level, Switchers, Lives, CurrentObj, LetChange, GameIsOver, Energy, MagnetSound;
struct sp_UDK Keys;
unsigned char LetFire, LetTrans;
unsigned int EnemiesDiff [] = {0, 1, 9 * 16, 9 * 16 + 1, 18 * 16, 18 * 16 + 1, 24 * 16};
unsigned int Scores [] = {50, 100, 250};
unsigned char SwitchsNeeded [9] = {0, 2, 3, 3, 5, 6, 6, /*7*/9, 10};
unsigned char SwitchLocation [9] = {10, 18, 48, 20, 41, 45, 65, 27, 60};

unsigned char pi, ka;
unsigned char Ax, Ay;

signed char Sent;

unsigned int af[] = {0, 1, 12 * 16,
                      192 + 1, 192 * 2, 192 * 2 + 1};


#include "binaries.h"

#include "../gfx/charset.h"

#asm
    DEFB      0, 0, 0, 0, 0, 0, 0, 0
    DEFB      0, 0, 0, 0, 0, 0, 0, 0
#endasm

#include "../gfx/astronaut.h"
#include "../gfx/astronaut2.h"
#include "../gfx/Enemies.h"
#include "../gfx/Explosion.h"
#include "../gfx/fire.h"
#include "../gfx/particles.h"
#include "../gfx/Arrows.h"
