@echo off
set path=%path%;\z88dk\bin;..\..\Pasmo;..\..\Compresores;..\..\utoloader0.4\bin
set Z80_OZFILES=\z88dk\Lib\
set ZCCCFG=\z88dk\Lib\Config\
del ..\bin\Cray-5.bin
del ..\bin\wyzplayer.z80
del ..\bin\asm_page3.bin
exomizer raw -o ..\bin\marcador.bin ..\gfx\marcador.scr
exomizer raw -o ..\bin\menu.bin ..\gfx\menu.scr
exomizer raw -o ..\bin\gameover.bin ..\gfx\gameover.scr
exomizer raw -o ..\bin\finished.bin ..\gfx\finished.scr
pasmo deexo.asm ..\bin\deexo.bin
pasmo ..\gfx\Panel.asm ..\bin\panel.bin
pasmo ..\gfx\Banner_TE.asm ..\bin\Banner_TE.bin
asmsx wyzplayer.asm
pasmo asm_page3.asm ..\bin\asm_page3.bin asm_page3.sym
zcc +zx -O3 -vn main.c -o ../bin/Cray-5.bin -lsplib2 -zorg=24963
del ~tmppre.*
buildtzx -l 1 -i template.txt -o Cray-5.tzx -n Cray-5
